// create separate namespace for all the Emscripten stuff.. otherwise naming clashes may occur especially when 
// optimizing using closure compiler..
window.spp_backend_state_AYUMI= {
	locateFile: function(path, scriptDirectory) { return (typeof window.WASM_SEARCH_PATH == 'undefined') ? path : window.WASM_SEARCH_PATH + path; },
	notReady: true,
	adapterCallback: function(){}	// overwritten later	
};
window.spp_backend_state_AYUMI["onRuntimeInitialized"] = function() {	// emscripten callback needed in case async init is used (e.g. for WASM)
	this.notReady= false;
	this.adapterCallback();
}.bind(window.spp_backend_state_AYUMI);

var backend_AYUMI = (function(Module) {
var d;d||(d=typeof Module !== 'undefined' ? Module : {});var k={},l;for(l in d)d.hasOwnProperty(l)&&(k[l]=d[l]);d.arguments=[];d.thisProgram="./this.program";d.quit=function(a,b){throw b;};d.preRun=[];d.postRun=[];var m=!1,n=!1,p=!1,aa=!1;m="object"===typeof window;n="function"===typeof importScripts;p="object"===typeof process&&"function"===typeof require&&!m&&!n;aa=!m&&!p&&!n;
if(d.ENVIRONMENT)throw Error("Module.ENVIRONMENT has been deprecated. To force the environment, use the ENVIRONMENT compile-time option (for example, -s ENVIRONMENT=web or -s ENVIRONMENT=node)");assert("undefined"===typeof d.memoryInitializerPrefixURL,"Module.memoryInitializerPrefixURL option was removed, use Module.locateFile instead");assert("undefined"===typeof d.pthreadMainPrefixURL,"Module.pthreadMainPrefixURL option was removed, use Module.locateFile instead");
assert("undefined"===typeof d.cdInitializerPrefixURL,"Module.cdInitializerPrefixURL option was removed, use Module.locateFile instead");assert("undefined"===typeof d.filePackagePrefixURL,"Module.filePackagePrefixURL option was removed, use Module.locateFile instead");var q="";function ba(a){return d.locateFile?d.locateFile(a,q):q+a}
if(p){q=__dirname+"/";var da,ea;d.read=function(a,b){da||(da=require("fs"));ea||(ea=require("path"));a=ea.normalize(a);a=da.readFileSync(a);return b?a:a.toString()};d.readBinary=function(a){a=d.read(a,!0);a.buffer||(a=new Uint8Array(a));assert(a.buffer);return a};1<process.argv.length&&(d.thisProgram=process.argv[1].replace(/\\/g,"/"));d.arguments=process.argv.slice(2);"undefined"!==typeof module&&(module.exports=d);process.on("uncaughtException",function(a){throw a;});process.on("unhandledRejection",
t);d.quit=function(a){process.exit(a)};d.inspect=function(){return"[Emscripten Module object]"}}else if(aa)"undefined"!=typeof read&&(d.read=function(a){return read(a)}),d.readBinary=function(a){if("function"===typeof readbuffer)return new Uint8Array(readbuffer(a));a=read(a,"binary");assert("object"===typeof a);return a},"undefined"!=typeof scriptArgs?d.arguments=scriptArgs:"undefined"!=typeof arguments&&(d.arguments=arguments),"function"===typeof quit&&(d.quit=function(a){quit(a)});else if(m||n)n?
q=self.location.href:document.currentScript&&(q=document.currentScript.src),q=0!==q.indexOf("blob:")?q.substr(0,q.lastIndexOf("/")+1):"",d.read=function(a){var b=new XMLHttpRequest;b.open("GET",a,!1);b.send(null);return b.responseText},n&&(d.readBinary=function(a){var b=new XMLHttpRequest;b.open("GET",a,!1);b.responseType="arraybuffer";b.send(null);return new Uint8Array(b.response)}),d.readAsync=function(a,b,c){var e=new XMLHttpRequest;e.open("GET",a,!0);e.responseType="arraybuffer";e.onload=function(){200==
e.status||0==e.status&&e.response?b(e.response):c()};e.onerror=c;e.send(null)},d.setWindowTitle=function(a){document.title=a};else throw Error("environment detection error");var fa=d.print||("undefined"!==typeof console?console.log.bind(console):"undefined"!==typeof print?print:null),u=d.printErr||("undefined"!==typeof printErr?printErr:"undefined"!==typeof console&&console.warn.bind(console)||fa);for(l in k)k.hasOwnProperty(l)&&(d[l]=k[l]);k=void 0;ha=ia=ja=function(){t("cannot use the stack before compiled code is ready to run, and has provided stack access")};
function ka(a){assert(!la);var b=x;x=x+a+15&-16;assert(x<y,"not enough memory for static allocation - increase TOTAL_MEMORY");return b}function ma(a){var b;b||(b=16);return Math.ceil(a/b)*b}var na,oa={"f64-rem":function(a,b){return a%b},"debugger":function(){debugger}},pa=!1;function assert(a,b){a||t("Assertion failed: "+b)}
var sa={stackSave:function(){ha()},stackRestore:function(){ia()},arrayToC:function(a){var b=ja(a.length);assert(0<=a.length,"writeArrayToMemory array must have a length (should be an array or typed array)");qa.set(a,b);return b},stringToC:function(a){var b=0;if(null!==a&&void 0!==a&&0!==a){var c=(a.length<<2)+1,e=b=ja(c);assert("number"==typeof c,"stringToUTF8(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!");ra(a,z,e,c)}return b}},ta={string:sa.stringToC,
array:sa.arrayToC};function va(a,b){if(0===b||!a)return"";for(var c=0,e,f=0;;){assert(a+f<y);e=z[a+f>>0];c|=e;if(0==e&&!b)break;f++;if(b&&f==b)break}b||(b=f);e="";if(128>c){for(;0<b;)c=String.fromCharCode.apply(String,z.subarray(a,a+Math.min(b,1024))),e=e?e+c:c,a+=1024,b-=1024;return e}return wa(a)}var xa="undefined"!==typeof TextDecoder?new TextDecoder("utf8"):void 0;
function ya(a,b){for(var c=b;a[c];)++c;if(16<c-b&&a.subarray&&xa)return xa.decode(a.subarray(b,c));for(c="";;){var e=a[b++];if(!e)return c;if(e&128){var f=a[b++]&63;if(192==(e&224))c+=String.fromCharCode((e&31)<<6|f);else{var g=a[b++]&63;if(224==(e&240))e=(e&15)<<12|f<<6|g;else{var h=a[b++]&63;if(240==(e&248))e=(e&7)<<18|f<<12|g<<6|h;else{var r=a[b++]&63;if(248==(e&252))e=(e&3)<<24|f<<18|g<<12|h<<6|r;else{var w=a[b++]&63;e=(e&1)<<30|f<<24|g<<18|h<<12|r<<6|w}}}65536>e?c+=String.fromCharCode(e):(e-=
65536,c+=String.fromCharCode(55296|e>>10,56320|e&1023))}}else c+=String.fromCharCode(e)}}function wa(a){return ya(z,a)}
function ra(a,b,c,e){if(!(0<e))return 0;var f=c;e=c+e-1;for(var g=0;g<a.length;++g){var h=a.charCodeAt(g);if(55296<=h&&57343>=h){var r=a.charCodeAt(++g);h=65536+((h&1023)<<10)|r&1023}if(127>=h){if(c>=e)break;b[c++]=h}else{if(2047>=h){if(c+1>=e)break;b[c++]=192|h>>6}else{if(65535>=h){if(c+2>=e)break;b[c++]=224|h>>12}else{if(2097151>=h){if(c+3>=e)break;b[c++]=240|h>>18}else{if(67108863>=h){if(c+4>=e)break;b[c++]=248|h>>24}else{if(c+5>=e)break;b[c++]=252|h>>30;b[c++]=128|h>>24&63}b[c++]=128|h>>18&63}b[c++]=
128|h>>12&63}b[c++]=128|h>>6&63}b[c++]=128|h&63}}b[c]=0;return c-f}"undefined"!==typeof TextDecoder&&new TextDecoder("utf-16le");function za(a){return a.replace(/__Z[\w\d_]+/g,function(a){na||(na={});na["warning: build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling"]||(na["warning: build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling"]=1,u("warning: build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling"));return a===a?a:a+" ["+a+"]"})}
function Aa(){a:{var a=Error();if(!a.stack){try{throw Error(0);}catch(b){a=b}if(!a.stack){a="(no stack trace available)";break a}}a=a.stack.toString()}d.extraStackTrace&&(a+="\n"+d.extraStackTrace());return za(a)}var buffer,qa,z,Ba,A,B;
function Ca(){d.HEAP8=qa=new Int8Array(buffer);d.HEAP16=Ba=new Int16Array(buffer);d.HEAP32=A=new Int32Array(buffer);d.HEAPU8=z=new Uint8Array(buffer);d.HEAPU16=new Uint16Array(buffer);d.HEAPU32=B=new Uint32Array(buffer);d.HEAPF32=new Float32Array(buffer);d.HEAPF64=new Float64Array(buffer)}var Da,x,la,Ea,Fa,C,Ga,D;Da=x=Ea=Fa=C=Ga=D=0;la=!1;
function Ha(){34821223==B[(C>>2)-1]&&2310721022==B[(C>>2)-2]||t("Stack overflow! Stack cookie has been overwritten, expected hex dwords 0x89BACDFE and 0x02135467, but received 0x"+B[(C>>2)-2].toString(16)+" "+B[(C>>2)-1].toString(16));if(1668509029!==A[0])throw"Runtime error: The application has corrupted its heap memory area (address zero)!";}
function Ia(){t("Cannot enlarge memory arrays. Either (1) compile with  -s TOTAL_MEMORY=X  with X higher than the current value "+y+", (2) compile with  -s ALLOW_MEMORY_GROWTH=1  which allows increasing the size at runtime, or (3) if you want malloc to return NULL (0) instead of this abort, compile with  -s ABORTING_MALLOC=0 ")}var Ja=d.TOTAL_STACK||5242880,y=d.TOTAL_MEMORY||16777216;y<Ja&&u("TOTAL_MEMORY should be larger than TOTAL_STACK, was "+y+"! (TOTAL_STACK="+Ja+")");
assert("undefined"!==typeof Int32Array&&"undefined"!==typeof Float64Array&&void 0!==Int32Array.prototype.subarray&&void 0!==Int32Array.prototype.set,"JS engine does not provide full typed array support");
d.buffer?(buffer=d.buffer,assert(buffer.byteLength===y,"provided buffer should be "+y+" bytes, but it is "+buffer.byteLength)):("object"===typeof WebAssembly&&"function"===typeof WebAssembly.Memory?(assert(0===y%65536),d.wasmMemory=new WebAssembly.Memory({initial:y/65536,maximum:y/65536}),buffer=d.wasmMemory.buffer):buffer=new ArrayBuffer(y),assert(buffer.byteLength===y),d.buffer=buffer);Ca();A[0]=1668509029;Ba[1]=25459;
if(115!==z[2]||99!==z[3])throw"Runtime error: expected the system to be little-endian!";function Ka(a){for(;0<a.length;){var b=a.shift();if("function"==typeof b)b();else{var c=b.Nc;"number"===typeof c?void 0===b.U?d.dynCall_v(c):d.dynCall_vi(c,b.U):c(void 0===b.U?null:b.U)}}}var La=[],Ma=[],Na=[],Pa=[],Qa=[],E=!1;function Ra(){var a=d.preRun.shift();La.unshift(a)}assert(Math.imul&&Math.fround&&Math.clz32&&Math.trunc,"this is a legacy browser, build with LEGACY_VM_SUPPORT");
var F=0,G=null,Sa=null,H={};function Ta(a){for(var b=a;H[a];)a=b+Math.random();return a}function Ua(a){F++;d.monitorRunDependencies&&d.monitorRunDependencies(F);a?(assert(!H[a]),H[a]=1,null===G&&"undefined"!==typeof setInterval&&(G=setInterval(function(){if(pa)clearInterval(G),G=null;else{var a=!1,c;for(c in H)a||(a=!0,u("still waiting on run dependencies:")),u("dependency: "+c);a&&u("(end of list)")}},1E4))):u("warning: run dependency added without ID")}
function Va(a){F--;d.monitorRunDependencies&&d.monitorRunDependencies(F);a?(assert(H[a]),delete H[a]):u("warning: run dependency removed without ID");0==F&&(null!==G&&(clearInterval(G),G=null),Sa&&(a=Sa,Sa=null,a()))}d.preloadedImages={};d.preloadedAudios={};function Wa(a){return String.prototype.startsWith?a.startsWith("data:application/octet-stream;base64,"):0===a.indexOf("data:application/octet-stream;base64,")}
(function(){function a(){try{if(d.wasmBinary)return new Uint8Array(d.wasmBinary);if(d.readBinary)return d.readBinary(f);throw"both async and sync fetching of the wasm failed";}catch(v){t(v)}}function b(){return d.wasmBinary||!m&&!n||"function"!==typeof fetch?new Promise(function(b){b(a())}):fetch(f,{credentials:"same-origin"}).then(function(a){if(!a.ok)throw"failed to load wasm binary file at '"+f+"'";return a.arrayBuffer()}).catch(function(){return a()})}function c(a){function c(a){r=a.exports;if(r.memory){a=
r.memory;var b=d.buffer;a.byteLength<b.byteLength&&u("the new buffer in mergeMemory is smaller than the previous one. in native wasm, we should grow memory here");b=new Int8Array(b);(new Int8Array(a)).set(b);d.buffer=buffer=a;Ca()}d.asm=r;d.usingWasm=!0;Va("wasm-instantiate")}function e(a){assert(d===v,"the Module object should not be replaced during async compilation - perhaps the order of HTML elements is wrong?");v=null;c(a.instance)}function g(a){b().then(function(a){return WebAssembly.instantiate(a,
h)}).then(a,function(a){u("failed to asynchronously prepare wasm: "+a);t(a)})}if("object"!==typeof WebAssembly)return t("No WebAssembly support found. Build with -s WASM=0 to target JavaScript instead."),u("no native wasm support detected"),!1;if(!(d.wasmMemory instanceof WebAssembly.Memory))return u("no native wasm Memory in use"),!1;a.memory=d.wasmMemory;h.global={NaN:NaN,Infinity:Infinity};h["global.Math"]=Math;h.env=a;Ua("wasm-instantiate");if(d.instantiateWasm)try{return d.instantiateWasm(h,
c)}catch(ca){return u("Module.instantiateWasm callback failed with error: "+ca),!1}var v=d;d.wasmBinary||"function"!==typeof WebAssembly.instantiateStreaming||Wa(f)||"function"!==typeof fetch?g(e):WebAssembly.instantiateStreaming(fetch(f,{credentials:"same-origin"}),h).then(e,function(a){u("wasm streaming compile failed: "+a);u("falling back to ArrayBuffer instantiation");g(e)});return{}}var e="ayumi.wast",f="ayumi.wasm",g="ayumi.temp.asm.js";Wa(e)||(e=ba(e));Wa(f)||(f=ba(f));Wa(g)||(g=ba(g));var h=
{global:null,env:null,asm2wasm:oa,parent:d},r=null;d.asmPreload=d.asm;var w=d.reallocBuffer;d.reallocBuffer=function(a){if("asmjs"===M)var b=w(a);else a:{var c=d.usingWasm?65536:16777216;0<a%c&&(a+=c-a%c);c=d.buffer.byteLength;if(d.usingWasm)try{b=-1!==d.wasmMemory.grow((a-c)/65536)?d.buffer=d.wasmMemory.buffer:null;break a}catch(Qb){console.error("Module.reallocBuffer: Attempted to grow from "+c+" bytes to "+a+" bytes, but got error: "+Qb);b=null;break a}b=void 0}return b};var M="";d.asm=function(a,
b){if(!b.table){a=d.wasmTableSize;void 0===a&&(a=1024);var e=d.wasmMaxTableSize;b.table="object"===typeof WebAssembly&&"function"===typeof WebAssembly.Table?void 0!==e?new WebAssembly.Table({initial:a,maximum:e,element:"anyfunc"}):new WebAssembly.Table({initial:a,element:"anyfunc"}):Array(a);d.wasmTable=b.table}b.memoryBase||(b.memoryBase=d.STATIC_BASE);b.tableBase||(b.tableBase=0);b=c(b);assert(b,"no binaryen method succeeded. consider enabling more options, like interpreting, if you want that: http://kripken.github.io/emscripten-site/docs/compiling/WebAssembly.html#binaryen-methods");
return b}})();Da=1024;x=Da+29184;Ma.push();d.STATIC_BASE=Da;d.STATIC_BUMP=29184;var Xa=x;x+=16;assert(0==Xa%8);
var I={D:1,s:2,xc:3,tb:4,v:5,fa:6,Ma:7,Rb:8,C:9,$a:10,aa:11,Hc:11,va:12,O:13,mb:14,cc:15,P:16,ba:17,Ic:18,S:19,da:20,L:21,l:22,Mb:23,ua:24,A:25,Ec:26,nb:27,Zb:28,T:29,uc:30,Fb:31,nc:32,jb:33,rc:34,Vb:42,qb:43,ab:44,wb:45,xb:46,yb:47,Eb:48,Fc:49,Pb:50,vb:51,gb:35,Sb:37,Sa:52,Va:53,Jc:54,Nb:55,Wa:56,Xa:57,hb:35,Ya:59,ac:60,Qb:61,Bc:62,$b:63,Wb:64,Xb:65,tc:66,Tb:67,Pa:68,yc:69,bb:70,oc:71,Hb:72,kb:73,Ua:74,ic:76,Ta:77,sc:78,zb:79,Ab:80,Db:81,Cb:82,Bb:83,bc:38,ea:39,Ib:36,R:40,jc:95,mc:96,fb:104,Ob:105,
Qa:97,qc:91,fc:88,Yb:92,vc:108,eb:111,Na:98,cb:103,Lb:101,Jb:100,Cc:110,ob:112,pb:113,sb:115,Ra:114,ib:89,Gb:90,pc:93,wc:94,Oa:99,Kb:102,ub:106,dc:107,Dc:109,Gc:87,lb:122,zc:116,hc:95,Ub:123,rb:84,kc:75,Za:125,ec:131,lc:130,Ac:86},Ya={0:"Success",1:"Not super-user",2:"No such file or directory",3:"No such process",4:"Interrupted system call",5:"I/O error",6:"No such device or address",7:"Arg list too long",8:"Exec format error",9:"Bad file number",10:"No children",11:"No more processes",12:"Not enough core",
13:"Permission denied",14:"Bad address",15:"Block device required",16:"Mount device busy",17:"File exists",18:"Cross-device link",19:"No such device",20:"Not a directory",21:"Is a directory",22:"Invalid argument",23:"Too many open files in system",24:"Too many open files",25:"Not a typewriter",26:"Text file busy",27:"File too large",28:"No space left on device",29:"Illegal seek",30:"Read only file system",31:"Too many links",32:"Broken pipe",33:"Math arg out of domain of func",34:"Math result not representable",
35:"File locking deadlock error",36:"File or path name too long",37:"No record locks available",38:"Function not implemented",39:"Directory not empty",40:"Too many symbolic links",42:"No message of desired type",43:"Identifier removed",44:"Channel number out of range",45:"Level 2 not synchronized",46:"Level 3 halted",47:"Level 3 reset",48:"Link number out of range",49:"Protocol driver not attached",50:"No CSI structure available",51:"Level 2 halted",52:"Invalid exchange",53:"Invalid request descriptor",
54:"Exchange full",55:"No anode",56:"Invalid request code",57:"Invalid slot",59:"Bad font file fmt",60:"Device not a stream",61:"No data (for no delay io)",62:"Timer expired",63:"Out of streams resources",64:"Machine is not on the network",65:"Package not installed",66:"The object is remote",67:"The link has been severed",68:"Advertise error",69:"Srmount error",70:"Communication error on send",71:"Protocol error",72:"Multihop attempted",73:"Cross mount point (not really error)",74:"Trying to read unreadable message",
75:"Value too large for defined data type",76:"Given log. name not unique",77:"f.d. invalid for this operation",78:"Remote address changed",79:"Can   access a needed shared lib",80:"Accessing a corrupted shared lib",81:".lib section in a.out corrupted",82:"Attempting to link in too many libs",83:"Attempting to exec a shared library",84:"Illegal byte sequence",86:"Streams pipe error",87:"Too many users",88:"Socket operation on non-socket",89:"Destination address required",90:"Message too long",91:"Protocol wrong type for socket",
92:"Protocol not available",93:"Unknown protocol",94:"Socket type not supported",95:"Not supported",96:"Protocol family not supported",97:"Address family not supported by protocol family",98:"Address already in use",99:"Address not available",100:"Network interface is not configured",101:"Network is unreachable",102:"Connection reset by network",103:"Connection aborted",104:"Connection reset by peer",105:"No buffer space available",106:"Socket is already connected",107:"Socket is not connected",108:"Can't send after socket shutdown",
109:"Too many references",110:"Connection timed out",111:"Connection refused",112:"Host is down",113:"Host is unreachable",114:"Socket already connected",115:"Connection already in progress",116:"Stale file handle",122:"Quota exceeded",123:"No medium (in tape drive)",125:"Operation canceled",130:"Previous owner died",131:"State not recoverable"};function Za(a){d.___errno_location?A[d.___errno_location()>>2]=a:u("failed to set errno from JS");return a}
function $a(a,b){for(var c=0,e=a.length-1;0<=e;e--){var f=a[e];"."===f?a.splice(e,1):".."===f?(a.splice(e,1),c++):c&&(a.splice(e,1),c--)}if(b)for(;c;c--)a.unshift("..");return a}function ab(a){var b="/"===a.charAt(0),c="/"===a.substr(-1);(a=$a(a.split("/").filter(function(a){return!!a}),!b).join("/"))||b||(a=".");a&&c&&(a+="/");return(b?"/":"")+a}
function bb(a){var b=/^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/.exec(a).slice(1);a=b[0];b=b[1];if(!a&&!b)return".";b&&(b=b.substr(0,b.length-1));return a+b}function cb(a){if("/"===a)return"/";var b=a.lastIndexOf("/");return-1===b?a:a.substr(b+1)}function db(){var a=Array.prototype.slice.call(arguments,0);return ab(a.join("/"))}function J(a,b){return ab(a+"/"+b)}
function eb(){for(var a="",b=!1,c=arguments.length-1;-1<=c&&!b;c--){b=0<=c?arguments[c]:"/";if("string"!==typeof b)throw new TypeError("Arguments to path.resolve must be strings");if(!b)return"";a=b+"/"+a;b="/"===b.charAt(0)}a=$a(a.split("/").filter(function(a){return!!a}),!b).join("/");return(b?"/":"")+a||"."}var fb=[];function gb(a,b){fb[a]={input:[],output:[],G:b};hb(a,ib)}
var ib={open:function(a){var b=fb[a.node.rdev];if(!b)throw new K(I.S);a.tty=b;a.seekable=!1},close:function(a){a.tty.G.flush(a.tty)},flush:function(a){a.tty.G.flush(a.tty)},read:function(a,b,c,e){if(!a.tty||!a.tty.G.oa)throw new K(I.fa);for(var f=0,g=0;g<e;g++){try{var h=a.tty.G.oa(a.tty)}catch(r){throw new K(I.v);}if(void 0===h&&0===f)throw new K(I.aa);if(null===h||void 0===h)break;f++;b[c+g]=h}f&&(a.node.timestamp=Date.now());return f},write:function(a,b,c,e){if(!a.tty||!a.tty.G.Z)throw new K(I.fa);
for(var f=0;f<e;f++)try{a.tty.G.Z(a.tty,b[c+f])}catch(g){throw new K(I.v);}e&&(a.node.timestamp=Date.now());return f}},kb={oa:function(a){if(!a.input.length){var b=null;if(p){var c=new Buffer(256),e=0,f=process.stdin.fd;if("win32"!=process.platform){var g=!1;try{f=fs.openSync("/dev/stdin","r"),g=!0}catch(h){}}try{e=fs.readSync(f,c,0,256,null)}catch(h){if(-1!=h.toString().indexOf("EOF"))e=0;else throw h;}g&&fs.closeSync(f);0<e?b=c.slice(0,e).toString("utf-8"):b=null}else"undefined"!=typeof window&&
"function"==typeof window.prompt?(b=window.prompt("Input: "),null!==b&&(b+="\n")):"function"==typeof readline&&(b=readline(),null!==b&&(b+="\n"));if(!b)return null;a.input=jb(b)}return a.input.shift()},Z:function(a,b){null===b||10===b?(fa(ya(a.output,0)),a.output=[]):0!=b&&a.output.push(b)},flush:function(a){a.output&&0<a.output.length&&(fa(ya(a.output,0)),a.output=[])}},lb={Z:function(a,b){null===b||10===b?(u(ya(a.output,0)),a.output=[]):0!=b&&a.output.push(b)},flush:function(a){a.output&&0<a.output.length&&
(u(ya(a.output,0)),a.output=[])}},L={m:null,i:function(){return L.createNode(null,"/",16895,0)},createNode:function(a,b,c,e){if(24576===(c&61440)||4096===(c&61440))throw new K(I.D);L.m||(L.m={dir:{node:{o:L.c.o,h:L.c.h,lookup:L.c.lookup,I:L.c.I,rename:L.c.rename,unlink:L.c.unlink,rmdir:L.c.rmdir,readdir:L.c.readdir,symlink:L.c.symlink},stream:{u:L.f.u}},file:{node:{o:L.c.o,h:L.c.h},stream:{u:L.f.u,read:L.f.read,write:L.f.write,ga:L.f.ga,ra:L.f.ra,ta:L.f.ta}},link:{node:{o:L.c.o,h:L.c.h,readlink:L.c.readlink},
stream:{}},ja:{node:{o:L.c.o,h:L.c.h},stream:mb}});c=nb(a,b,c,e);N(c.mode)?(c.c=L.m.dir.node,c.f=L.m.dir.stream,c.b={}):32768===(c.mode&61440)?(c.c=L.m.file.node,c.f=L.m.file.stream,c.g=0,c.b=null):40960===(c.mode&61440)?(c.c=L.m.link.node,c.f=L.m.link.stream):8192===(c.mode&61440)&&(c.c=L.m.ja.node,c.f=L.m.ja.stream);c.timestamp=Date.now();a&&(a.b[b]=c);return c},Ca:function(a){if(a.b&&a.b.subarray){for(var b=[],c=0;c<a.g;++c)b.push(a.b[c]);return b}return a.b},Oc:function(a){return a.b?a.b.subarray?
a.b.subarray(0,a.g):new Uint8Array(a.b):new Uint8Array},ka:function(a,b){a.b&&a.b.subarray&&b>a.b.length&&(a.b=L.Ca(a),a.g=a.b.length);if(!a.b||a.b.subarray){var c=a.b?a.b.length:0;c>=b||(b=Math.max(b,c*(1048576>c?2:1.125)|0),0!=c&&(b=Math.max(b,256)),c=a.b,a.b=new Uint8Array(b),0<a.g&&a.b.set(c.subarray(0,a.g),0))}else for(!a.b&&0<b&&(a.b=[]);a.b.length<b;)a.b.push(0)},Ha:function(a,b){if(a.g!=b)if(0==b)a.b=null,a.g=0;else{if(!a.b||a.b.subarray){var c=a.b;a.b=new Uint8Array(new ArrayBuffer(b));c&&
a.b.set(c.subarray(0,Math.min(b,a.g)))}else if(a.b||(a.b=[]),a.b.length>b)a.b.length=b;else for(;a.b.length<b;)a.b.push(0);a.g=b}},c:{o:function(a){var b={};b.dev=8192===(a.mode&61440)?a.id:1;b.ino=a.id;b.mode=a.mode;b.nlink=1;b.uid=0;b.gid=0;b.rdev=a.rdev;N(a.mode)?b.size=4096:32768===(a.mode&61440)?b.size=a.g:40960===(a.mode&61440)?b.size=a.link.length:b.size=0;b.atime=new Date(a.timestamp);b.mtime=new Date(a.timestamp);b.ctime=new Date(a.timestamp);b.B=4096;b.blocks=Math.ceil(b.size/b.B);return b},
h:function(a,b){void 0!==b.mode&&(a.mode=b.mode);void 0!==b.timestamp&&(a.timestamp=b.timestamp);void 0!==b.size&&L.Ha(a,b.size)},lookup:function(){throw ob[I.s];},I:function(a,b,c,e){return L.createNode(a,b,c,e)},rename:function(a,b,c){if(N(a.mode)){try{var e=O(b,c)}catch(g){}if(e)for(var f in e.b)throw new K(I.ea);}delete a.parent.b[a.name];a.name=c;b.b[c]=a;a.parent=b},unlink:function(a,b){delete a.b[b]},rmdir:function(a,b){var c=O(a,b),e;for(e in c.b)throw new K(I.ea);delete a.b[b]},readdir:function(a){var b=
[".",".."],c;for(c in a.b)a.b.hasOwnProperty(c)&&b.push(c);return b},symlink:function(a,b,c){a=L.createNode(a,b,41471,0);a.link=c;return a},readlink:function(a){if(40960!==(a.mode&61440))throw new K(I.l);return a.link}},f:{read:function(a,b,c,e,f){var g=a.node.b;if(f>=a.node.g)return 0;a=Math.min(a.node.g-f,e);assert(0<=a);if(8<a&&g.subarray)b.set(g.subarray(f,f+a),c);else for(e=0;e<a;e++)b[c+e]=g[f+e];return a},write:function(a,b,c,e,f,g){if(!e)return 0;a=a.node;a.timestamp=Date.now();if(b.subarray&&
(!a.b||a.b.subarray)){if(g)return assert(0===f,"canOwn must imply no weird position inside the file"),a.b=b.subarray(c,c+e),a.g=e;if(0===a.g&&0===f)return a.b=new Uint8Array(b.subarray(c,c+e)),a.g=e;if(f+e<=a.g)return a.b.set(b.subarray(c,c+e),f),e}L.ka(a,f+e);if(a.b.subarray&&b.subarray)a.b.set(b.subarray(c,c+e),f);else for(g=0;g<e;g++)a.b[f+g]=b[c+g];a.g=Math.max(a.g,f+e);return e},u:function(a,b,c){1===c?b+=a.position:2===c&&32768===(a.node.mode&61440)&&(b+=a.node.g);if(0>b)throw new K(I.l);return b},
ga:function(a,b,c){L.ka(a.node,b+c);a.node.g=Math.max(a.node.g,b+c)},ra:function(a,b,c,e,f,g,h){if(32768!==(a.node.mode&61440))throw new K(I.S);c=a.node.b;if(h&2||c.buffer!==b&&c.buffer!==b.buffer){if(0<f||f+e<a.node.g)c.subarray?c=c.subarray(f,f+e):c=Array.prototype.slice.call(c,f,f+e);a=!0;e=pb(e);if(!e)throw new K(I.va);b.set(c,e)}else a=!1,e=c.byteOffset;return{Qc:e,Kc:a}},ta:function(a,b,c,e,f){if(32768!==(a.node.mode&61440))throw new K(I.S);if(f&2)return 0;L.f.write(a,b,0,e,c,!1);return 0}}},
P={N:!1,Ka:function(){P.N=!!process.platform.match(/^win/);var a=process.binding("constants");a.fs&&(a=a.fs);P.la={1024:a.O_APPEND,64:a.O_CREAT,128:a.O_EXCL,0:a.O_RDONLY,2:a.O_RDWR,4096:a.O_SYNC,512:a.O_TRUNC,1:a.O_WRONLY}},ha:function(a){return Buffer.Mc?Buffer.from(a):new Buffer(a)},i:function(a){assert(p);return P.createNode(null,"/",P.na(a.Y.root),0)},createNode:function(a,b,c){if(!N(c)&&32768!==(c&61440)&&40960!==(c&61440))throw new K(I.l);a=nb(a,b,c);a.c=P.c;a.f=P.f;return a},na:function(a){try{var b=
fs.lstatSync(a);P.N&&(b.mode=b.mode|(b.mode&292)>>2)}catch(c){if(!c.code)throw c;throw new K(I[c.code]);}return b.mode},j:function(a){for(var b=[];a.parent!==a;)b.push(a.name),a=a.parent;b.push(a.i.Y.root);b.reverse();return db.apply(null,b)},Ba:function(a){a&=-2656257;var b=0,c;for(c in P.la)a&c&&(b|=P.la[c],a^=c);if(a)throw new K(I.l);return b},c:{o:function(a){a=P.j(a);try{var b=fs.lstatSync(a)}catch(c){if(!c.code)throw c;throw new K(I[c.code]);}P.N&&!b.B&&(b.B=4096);P.N&&!b.blocks&&(b.blocks=
(b.size+b.B-1)/b.B|0);return{dev:b.dev,ino:b.ino,mode:b.mode,nlink:b.nlink,uid:b.uid,gid:b.gid,rdev:b.rdev,size:b.size,atime:b.atime,mtime:b.mtime,ctime:b.ctime,B:b.B,blocks:b.blocks}},h:function(a,b){var c=P.j(a);try{void 0!==b.mode&&(fs.chmodSync(c,b.mode),a.mode=b.mode),void 0!==b.size&&fs.truncateSync(c,b.size)}catch(e){if(!e.code)throw e;throw new K(I[e.code]);}},lookup:function(a,b){var c=J(P.j(a),b);c=P.na(c);return P.createNode(a,b,c)},I:function(a,b,c,e){a=P.createNode(a,b,c,e);b=P.j(a);
try{N(a.mode)?fs.mkdirSync(b,a.mode):fs.writeFileSync(b,"",{mode:a.mode})}catch(f){if(!f.code)throw f;throw new K(I[f.code]);}return a},rename:function(a,b,c){a=P.j(a);b=J(P.j(b),c);try{fs.renameSync(a,b)}catch(e){if(!e.code)throw e;throw new K(I[e.code]);}},unlink:function(a,b){a=J(P.j(a),b);try{fs.unlinkSync(a)}catch(c){if(!c.code)throw c;throw new K(I[c.code]);}},rmdir:function(a,b){a=J(P.j(a),b);try{fs.rmdirSync(a)}catch(c){if(!c.code)throw c;throw new K(I[c.code]);}},readdir:function(a){a=P.j(a);
try{return fs.readdirSync(a)}catch(b){if(!b.code)throw b;throw new K(I[b.code]);}},symlink:function(a,b,c){a=J(P.j(a),b);try{fs.symlinkSync(c,a)}catch(e){if(!e.code)throw e;throw new K(I[e.code]);}},readlink:function(a){var b=P.j(a);try{return b=fs.readlinkSync(b),b=qb.relative(qb.resolve(a.i.Y.root),b)}catch(c){if(!c.code)throw c;throw new K(I[c.code]);}}},f:{open:function(a){var b=P.j(a.node);try{32768===(a.node.mode&61440)&&(a.K=fs.openSync(b,P.Ba(a.flags)))}catch(c){if(!c.code)throw c;throw new K(I[c.code]);
}},close:function(a){try{32768===(a.node.mode&61440)&&a.K&&fs.closeSync(a.K)}catch(b){if(!b.code)throw b;throw new K(I[b.code]);}},read:function(a,b,c,e,f){if(0===e)return 0;try{return fs.readSync(a.K,P.ha(b.buffer),c,e,f)}catch(g){throw new K(I[g.code]);}},write:function(a,b,c,e,f){try{return fs.writeSync(a.K,P.ha(b.buffer),c,e,f)}catch(g){throw new K(I[g.code]);}},u:function(a,b,c){if(1===c)b+=a.position;else if(2===c&&32768===(a.node.mode&61440))try{b+=fs.fstatSync(a.K).size}catch(e){throw new K(I[e.code]);
}if(0>b)throw new K(I.l);return b}}};x+=16;x+=16;x+=16;var rb=null,sb={},Q=[],tb=1,R=null,ub=!0,S={},K=null,ob={};
function T(a,b){a=eb("/",a);b=b||{};if(!a)return{path:"",node:null};var c={ma:!0,$:0},e;for(e in c)void 0===b[e]&&(b[e]=c[e]);if(8<b.$)throw new K(I.R);a=$a(a.split("/").filter(function(a){return!!a}),!1);var f=rb;c="/";for(e=0;e<a.length;e++){var g=e===a.length-1;if(g&&b.parent)break;f=O(f,a[e]);c=J(c,a[e]);f.J&&(!g||g&&b.ma)&&(f=f.J.root);if(!g||b.V)for(g=0;40960===(f.mode&61440);)if(f=vb(c),c=eb(bb(c),f),f=T(c,{$:b.$}).node,40<g++)throw new K(I.R);}return{path:c,node:f}}
function U(a){for(var b;;){if(a===a.parent)return a=a.i.sa,b?"/"!==a[a.length-1]?a+"/"+b:a+b:a;b=b?a.name+"/"+b:a.name;a=a.parent}}function wb(a,b){for(var c=0,e=0;e<b.length;e++)c=(c<<5)-c+b.charCodeAt(e)|0;return(a+c>>>0)%R.length}function xb(a){var b=wb(a.parent.id,a.name);a.F=R[b];R[b]=a}function O(a,b){var c;if(c=(c=yb(a,"x"))?c:a.c.lookup?0:I.O)throw new K(c,a);for(c=R[wb(a.id,b)];c;c=c.F){var e=c.name;if(c.parent.id===a.id&&e===b)return c}return a.c.lookup(a,b)}
function nb(a,b,c,e){zb||(zb=function(a,b,c,e){a||(a=this);this.parent=a;this.i=a.i;this.J=null;this.id=tb++;this.name=b;this.mode=c;this.c={};this.f={};this.rdev=e},zb.prototype={},Object.defineProperties(zb.prototype,{read:{get:function(){return 365===(this.mode&365)},set:function(a){a?this.mode|=365:this.mode&=-366}},write:{get:function(){return 146===(this.mode&146)},set:function(a){a?this.mode|=146:this.mode&=-147}},Fa:{get:function(){return N(this.mode)}},Ea:{get:function(){return 8192===(this.mode&
61440)}}}));a=new zb(a,b,c,e);xb(a);return a}function N(a){return 16384===(a&61440)}var Ab={r:0,rs:1052672,"r+":2,w:577,wx:705,xw:705,"w+":578,"wx+":706,"xw+":706,a:1089,ax:1217,xa:1217,"a+":1090,"ax+":1218,"xa+":1218};function Bb(a){var b=["r","w","rw"][a&3];a&512&&(b+="w");return b}function yb(a,b){if(ub)return 0;if(-1===b.indexOf("r")||a.mode&292){if(-1!==b.indexOf("w")&&!(a.mode&146)||-1!==b.indexOf("x")&&!(a.mode&73))return I.O}else return I.O;return 0}
function Cb(a,b){try{return O(a,b),I.ba}catch(c){}return yb(a,"wx")}function Db(){var a=4096;for(var b=0;b<=a;b++)if(!Q[b])return b;throw new K(I.ua);}function Eb(a){Fb||(Fb=function(){},Fb.prototype={},Object.defineProperties(Fb.prototype,{object:{get:function(){return this.node},set:function(a){this.node=a}}}));var b=new Fb,c;for(c in a)b[c]=a[c];a=b;b=Db();a.fd=b;return Q[b]=a}var mb={open:function(a){a.f=sb[a.node.rdev].f;a.f.open&&a.f.open(a)},u:function(){throw new K(I.T);}};
function hb(a,b){sb[a]={f:b}}function Gb(a,b){var c="/"===b,e=!b;if(c&&rb)throw new K(I.P);if(!c&&!e){var f=T(b,{ma:!1});b=f.path;f=f.node;if(f.J)throw new K(I.P);if(!N(f.mode))throw new K(I.da);}b={type:a,Y:{},sa:b,Ga:[]};a=a.i(b);a.i=b;b.root=a;c?rb=a:f&&(f.J=b,f.i&&f.i.Ga.push(b))}function Hb(a,b,c){var e=T(a,{parent:!0}).node;a=cb(a);if(!a||"."===a||".."===a)throw new K(I.l);var f=Cb(e,a);if(f)throw new K(f);if(!e.c.I)throw new K(I.D);return e.c.I(e,a,b,c)}
function V(a,b){return Hb(a,(void 0!==b?b:511)&1023|16384,0)}function Ib(a,b,c){"undefined"===typeof c&&(c=b,b=438);return Hb(a,b|8192,c)}function Jb(a,b){if(!eb(a))throw new K(I.s);var c=T(b,{parent:!0}).node;if(!c)throw new K(I.s);b=cb(b);var e=Cb(c,b);if(e)throw new K(e);if(!c.c.symlink)throw new K(I.D);return c.c.symlink(c,b,a)}
function Kb(a){var b=T(a,{parent:!0}).node,c=cb(a),e=O(b,c);a:{try{var f=O(b,c)}catch(h){f=h.H;break a}var g=yb(b,"wx");f=g?g:N(f.mode)?I.L:0}if(f)throw new K(f);if(!b.c.unlink)throw new K(I.D);if(e.J)throw new K(I.P);try{S.willDeletePath&&S.willDeletePath(a)}catch(h){console.log("FS.trackingDelegate['willDeletePath']('"+a+"') threw an exception: "+h.message)}b.c.unlink(b,c);b=wb(e.parent.id,e.name);if(R[b]===e)R[b]=e.F;else for(b=R[b];b;){if(b.F===e){b.F=e.F;break}b=b.F}try{if(S.onDeletePath)S.onDeletePath(a)}catch(h){console.log("FS.trackingDelegate['onDeletePath']('"+
a+"') threw an exception: "+h.message)}}function vb(a){a=T(a).node;if(!a)throw new K(I.s);if(!a.c.readlink)throw new K(I.l);return eb(U(a.parent),a.c.readlink(a))}function Lb(a,b){var c;"string"===typeof a?c=T(a,{V:!0}).node:c=a;if(!c.c.h)throw new K(I.D);c.c.h(c,{mode:b&4095|c.mode&-4096,timestamp:Date.now()})}
function Mb(a,b){if(""===a)throw new K(I.s);if("string"===typeof b){var c=Ab[b];if("undefined"===typeof c)throw Error("Unknown file open mode: "+b);b=c}var e=b&64?("undefined"===typeof e?438:e)&4095|32768:0;if("object"===typeof a)var f=a;else{a=ab(a);try{f=T(a,{V:!(b&131072)}).node}catch(r){}}c=!1;if(b&64)if(f){if(b&128)throw new K(I.ba);}else f=Hb(a,e,0),c=!0;if(!f)throw new K(I.s);8192===(f.mode&61440)&&(b&=-513);if(b&65536&&!N(f.mode))throw new K(I.da);if(!c){var g=f?40960===(f.mode&61440)?I.R:
N(f.mode)&&("r"!==Bb(b)||b&512)?I.L:yb(f,Bb(b)):I.s;if(g)throw new K(g);}if(b&512){e=f;var h;"string"===typeof e?h=T(e,{V:!0}).node:h=e;if(!h.c.h)throw new K(I.D);if(N(h.mode))throw new K(I.L);if(32768!==(h.mode&61440))throw new K(I.l);if(e=yb(h,"w"))throw new K(e);h.c.h(h,{size:0,timestamp:Date.now()})}b&=-641;f=Eb({node:f,path:U(f),flags:b,seekable:!0,position:0,f:f.f,La:[],error:!1});f.f.open&&f.f.open(f);!d.logReadFiles||b&1||(Nb||(Nb={}),a in Nb||(Nb[a]=1,g("read file: "+a)));try{S.onOpenFile&&
(g=0,1!==(b&2097155)&&(g|=1),0!==(b&2097155)&&(g|=2),S.onOpenFile(a,g))}catch(r){console.log("FS.trackingDelegate['onOpenFile']('"+a+"', flags) threw an exception: "+r.message)}return f}function Ob(a){if(null===a.fd)throw new K(I.C);a.W&&(a.W=null);try{a.f.close&&a.f.close(a)}catch(b){throw b;}finally{Q[a.fd]=null}a.fd=null}function Pb(a,b,c){if(null===a.fd)throw new K(I.C);if(!a.seekable||!a.f.u)throw new K(I.T);a.position=a.f.u(a,b,c);a.La=[]}
function Rb(a,b,c,e,f,g){if(0>e||0>f)throw new K(I.l);if(null===a.fd)throw new K(I.C);if(0===(a.flags&2097155))throw new K(I.C);if(N(a.node.mode))throw new K(I.L);if(!a.f.write)throw new K(I.l);a.flags&1024&&Pb(a,0,2);var h="undefined"!==typeof f;if(!h)f=a.position;else if(!a.seekable)throw new K(I.T);b=a.f.write(a,b,c,e,f,g);h||(a.position+=b);try{if(a.path&&S.onWriteToFile)S.onWriteToFile(a.path)}catch(r){console.log("FS.trackingDelegate['onWriteToFile']('"+path+"') threw an exception: "+r.message)}return b}
function Sb(){K||(K=function(a,b){this.node=b;this.Ja=function(a){this.H=a;for(var b in I)if(I[b]===a){this.code=b;break}};this.Ja(a);this.message=Ya[a];this.stack&&Object.defineProperty(this,"stack",{value:Error().stack,writable:!0});this.stack&&(this.stack=za(this.stack))},K.prototype=Error(),K.prototype.constructor=K,[I.s].forEach(function(a){ob[a]=new K(a);ob[a].stack="<generic error, no stack>"}))}var Tb;function Ub(a,b){var c=0;a&&(c|=365);b&&(c|=146);return c}
function Vb(a,b,c,e){a=J("string"===typeof a?a:U(a),b);return V(a,Ub(c,e))}function Wb(a,b){a="string"===typeof a?a:U(a);for(b=b.split("/").reverse();b.length;){var c=b.pop();if(c){var e=J(a,c);try{V(e)}catch(f){}a=e}}return e}function Xb(a,b,c,e){a=J("string"===typeof a?a:U(a),b);c=Ub(c,e);return Hb(a,(void 0!==c?c:438)&4095|32768,0)}
function Yb(a,b,c,e,f,g){a=b?J("string"===typeof a?a:U(a),b):a;e=Ub(e,f);f=Hb(a,(void 0!==e?e:438)&4095|32768,0);if(c){if("string"===typeof c){a=Array(c.length);b=0;for(var h=c.length;b<h;++b)a[b]=c.charCodeAt(b);c=a}Lb(f,e|146);a=Mb(f,"w");Rb(a,c,0,c.length,0,g);Ob(a);Lb(f,e)}return f}
function W(a,b,c,e){a=J("string"===typeof a?a:U(a),b);b=Ub(!!c,!!e);W.qa||(W.qa=64);var f=W.qa++<<8|0;hb(f,{open:function(a){a.seekable=!1},close:function(){e&&e.buffer&&e.buffer.length&&e(10)},read:function(a,b,e,f){for(var g=0,h=0;h<f;h++){try{var r=c()}catch(ua){throw new K(I.v);}if(void 0===r&&0===g)throw new K(I.aa);if(null===r||void 0===r)break;g++;b[e+h]=r}g&&(a.node.timestamp=Date.now());return g},write:function(a,b,c,f){for(var g=0;g<f;g++)try{e(b[c+g])}catch(v){throw new K(I.v);}f&&(a.node.timestamp=
Date.now());return g}});return Ib(a,b,f)}function Zb(a,b,c){a=J("string"===typeof a?a:U(a),b);return Jb(c,a)}
function $b(a){if(a.Ea||a.Fa||a.link||a.b)return!0;var b=!0;if("undefined"!==typeof XMLHttpRequest)throw Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");if(d.read)try{a.b=jb(d.read(a.url)),a.g=a.b.length}catch(c){b=!1}else throw Error("Cannot load without read() or XMLHttpRequest.");b||Za(I.v);return b}
function ac(a,b,c,e,f){function g(){this.X=!1;this.M=[]}g.prototype.get=function(a){if(!(a>this.length-1||0>a)){var b=a%this.chunkSize;return this.pa(a/this.chunkSize|0)[b]}};g.prototype.Ia=function(a){this.pa=a};g.prototype.ia=function(){var a=new XMLHttpRequest;a.open("HEAD",c,!1);a.send(null);if(!(200<=a.status&&300>a.status||304===a.status))throw Error("Couldn't load "+c+". Status: "+a.status);var b=Number(a.getResponseHeader("Content-length")),e,f=(e=a.getResponseHeader("Accept-Ranges"))&&"bytes"===
e;a=(e=a.getResponseHeader("Content-Encoding"))&&"gzip"===e;var g=1048576;f||(g=b);var h=this;h.Ia(function(a){var e=a*g,f=(a+1)*g-1;f=Math.min(f,b-1);if("undefined"===typeof h.M[a]){var r=h.M;if(e>f)throw Error("invalid range ("+e+", "+f+") or no bytes requested!");if(f>b-1)throw Error("only "+b+" bytes available! programmer error!");var v=new XMLHttpRequest;v.open("GET",c,!1);b!==g&&v.setRequestHeader("Range","bytes="+e+"-"+f);"undefined"!=typeof Uint8Array&&(v.responseType="arraybuffer");v.overrideMimeType&&
v.overrideMimeType("text/plain; charset=x-user-defined");v.send(null);if(!(200<=v.status&&300>v.status||304===v.status))throw Error("Couldn't load "+c+". Status: "+v.status);e=void 0!==v.response?new Uint8Array(v.response||[]):jb(v.responseText||"");r[a]=e}if("undefined"===typeof h.M[a])throw Error("doXHR failed!");return h.M[a]});if(a||!b)g=b=1,g=b=this.pa(0).length,console.log("LazyFiles on gzip forces download of the whole file when length is accessed");this.ya=b;this.wa=g;this.X=!0};if("undefined"!==
typeof XMLHttpRequest){if(!n)throw"Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc";var h=new g;Object.defineProperties(h,{length:{get:function(){this.X||this.ia();return this.ya}},chunkSize:{get:function(){this.X||this.ia();return this.wa}}});var r=void 0}else r=c,h=void 0;var w=Xb(a,b,e,f);h?w.b=h:r&&(w.b=null,w.url=r);Object.defineProperties(w,{g:{get:function(){return this.b.length}}});var M={};Object.keys(w.f).forEach(function(a){var b=
w.f[a];M[a]=function(){if(!$b(w))throw new K(I.v);return b.apply(null,arguments)}});M.read=function(a,b,c,e,f){if(!$b(w))throw new K(I.v);a=a.node.b;if(f>=a.length)return 0;e=Math.min(a.length-f,e);assert(0<=e);if(a.slice)for(var g=0;g<e;g++)b[c+g]=a[f+g];else for(g=0;g<e;g++)b[c+g]=a.get(f+g);return e};w.f=M;return w}
function bc(a,b,c,e,f,g,h,r,w,M){function v(c){function v(c){M&&M();r||Yb(a,b,c,e,f,w);g&&g();Va(ua)}var ca=!1;d.preloadPlugins.forEach(function(a){!ca&&a.canHandle(Oa)&&(a.handle(c,Oa,v,function(){h&&h();Va(ua)}),ca=!0)});ca||v(c)}Browser.Pc();var Oa=b?eb(J(a,b)):a,ua=Ta("cp "+Oa);Ua(ua);"string"==typeof c?Browser.Lc(c,function(a){v(a)},h):v(c)}var FS={},zb,Fb,Nb,X=0;function Y(){X+=4;return A[X-4>>2]}function cc(){var a=Q[Y()];if(!a)throw new K(I.C);return a}Sb();R=Array(4096);Gb(L,"/");V("/tmp");
V("/home");V("/home/web_user");(function(){V("/dev");hb(259,{read:function(){return 0},write:function(a,b,f,g){return g}});Ib("/dev/null",259);gb(1280,kb);gb(1536,lb);Ib("/dev/tty",1280);Ib("/dev/tty1",1536);if("undefined"!==typeof crypto){var a=new Uint8Array(1);var b=function(){crypto.getRandomValues(a);return a[0]}}else p?b=function(){return require("crypto").randomBytes(1)[0]}:b=function(){t("random_device")};W("/dev","random",b);W("/dev","urandom",b);V("/dev/shm");V("/dev/shm/tmp")})();V("/proc");
V("/proc/self");V("/proc/self/fd");Gb({i:function(){var a=nb("/proc/self","fd",16895,73);a.c={lookup:function(a,c){var b=Q[+c];if(!b)throw new K(I.C);a={parent:null,i:{sa:"fake"},c:{readlink:function(){return b.path}}};return a.parent=a}};return a}},"/proc/self/fd");
Ma.unshift(function(){if(!d.noFSInit&&!Tb){assert(!Tb,"FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)");Tb=!0;Sb();d.stdin=d.stdin;d.stdout=d.stdout;d.stderr=d.stderr;d.stdin?W("/dev","stdin",d.stdin):Jb("/dev/tty","/dev/stdin");d.stdout?W("/dev","stdout",null,d.stdout):Jb("/dev/tty","/dev/stdout");d.stderr?W("/dev","stderr",null,d.stderr):Jb("/dev/tty1","/dev/stderr");var a=
Mb("/dev/stdin","r");assert(0===a.fd,"invalid handle for stdin ("+a.fd+")");a=Mb("/dev/stdout","w");assert(1===a.fd,"invalid handle for stdout ("+a.fd+")");a=Mb("/dev/stderr","w");assert(2===a.fd,"invalid handle for stderr ("+a.fd+")")}});Na.push(function(){ub=!1});Pa.push(function(){Tb=!1;var a=d._fflush;a&&a(0);for(a=0;a<Q.length;a++){var b=Q[a];b&&Ob(b)}});d.FS_createFolder=Vb;d.FS_createPath=Wb;d.FS_createDataFile=Yb;d.FS_createPreloadedFile=bc;d.FS_createLazyFile=ac;d.FS_createLink=Zb;
d.FS_createDevice=W;d.FS_unlink=Kb;Ma.unshift(function(){});Pa.push(function(){});if(p){var fs=require("fs"),qb=require("path");P.Ka()}D=ka(4);Ea=Fa=ma(x);C=Ea+Ja;Ga=ma(C);A[D>>2]=Ga;la=!0;assert(Ga<y,"TOTAL_MEMORY not big enough for stack");
function jb(a){for(var b=0,c=0;c<a.length;++c){var e=a.charCodeAt(c);55296<=e&&57343>=e&&(e=65536+((e&1023)<<10)|a.charCodeAt(++c)&1023);127>=e?++b:b=2047>=e?b+2:65535>=e?b+3:2097151>=e?b+4:67108863>=e?b+5:b+6}b=Array(b+1);a=ra(a,b,0,b.length);b.length=a;return b}d.wasmTableSize=17;d.wasmMaxTableSize=17;d.za={};
d.Aa={enlargeMemory:function(){Ia()},getTotalMemory:function(){return y},abortOnCannotGrowMemory:Ia,abortStackOverflow:function(a){t("Stack overflow! Attempted to allocate "+a+" bytes on the stack, but stack has only "+(C-ha()+a)+" bytes available!")},nullFunc_ii:function(a){u("Invalid function pointer called with signature 'ii'. Perhaps this is an invalid value (e.g. caused by calling a virtual method on a NULL pointer)? Or calling a function with an incorrect type, which will fail? (it is worth building your source files with -Werror (warnings are errors), as warnings can indicate undefined behavior which can cause this)");
u("Build with ASSERTIONS=2 for more info.");t(a)},nullFunc_iiii:function(a){u("Invalid function pointer called with signature 'iiii'. Perhaps this is an invalid value (e.g. caused by calling a virtual method on a NULL pointer)? Or calling a function with an incorrect type, which will fail? (it is worth building your source files with -Werror (warnings are errors), as warnings can indicate undefined behavior which can cause this)");u("Build with ASSERTIONS=2 for more info.");t(a)},nullFunc_v:function(a){u("Invalid function pointer called with signature 'v'. Perhaps this is an invalid value (e.g. caused by calling a virtual method on a NULL pointer)? Or calling a function with an incorrect type, which will fail? (it is worth building your source files with -Werror (warnings are errors), as warnings can indicate undefined behavior which can cause this)");
u("Build with ASSERTIONS=2 for more info.");t(a)},nullFunc_vi:function(a){u("Invalid function pointer called with signature 'vi'. Perhaps this is an invalid value (e.g. caused by calling a virtual method on a NULL pointer)? Or calling a function with an incorrect type, which will fail? (it is worth building your source files with -Werror (warnings are errors), as warnings can indicate undefined behavior which can cause this)");u("Build with ASSERTIONS=2 for more info.");t(a)},___lock:function(){},
___setErrNo:Za,___syscall140:function(a,b){X=b;try{var c=cc();Y();var e=Y(),f=Y(),g=Y();Pb(c,e,g);A[f>>2]=c.position;c.W&&0===e&&0===g&&(c.W=null);return 0}catch(h){return"undefined"!==typeof FS&&h instanceof K||t(h),-h.H}},___syscall146:function(a,b){X=b;try{var c=cc(),e=Y();a:{var f=Y();for(b=a=0;b<f;b++){var g=Rb(c,qa,A[e+8*b>>2],A[e+(8*b+4)>>2],void 0);if(0>g){var h=-1;break a}a+=g}h=a}return h}catch(r){return"undefined"!==typeof FS&&r instanceof K||t(r),-r.H}},___syscall54:function(a,b){X=b;
try{var c=cc(),e=Y();switch(e){case 21509:case 21505:return c.tty?0:-I.A;case 21510:case 21511:case 21512:case 21506:case 21507:case 21508:return c.tty?0:-I.A;case 21519:if(!c.tty)return-I.A;var f=Y();return A[f>>2]=0;case 21520:return c.tty?-I.l:-I.A;case 21531:a=f=Y();if(!c.f.Da)throw new K(I.A);return c.f.Da(c,e,a);case 21523:return c.tty?0:-I.A;case 21524:return c.tty?0:-I.A;default:t("bad ioctl syscall "+e)}}catch(g){return"undefined"!==typeof FS&&g instanceof K||t(g),-g.H}},___syscall6:function(a,
b){X=b;try{var c=cc();Ob(c);return 0}catch(e){return"undefined"!==typeof FS&&e instanceof K||t(e),-e.H}},___unlock:function(){},_abort:function(){d.abort()},_emscripten_memcpy_big:function(a,b,c){z.set(z.subarray(b,b+c),a);return a},DYNAMICTOP_PTR:D,STACKTOP:Fa,STACK_MAX:C};var Z=d.asm(d.za,d.Aa,buffer),dc=Z.___errno_location;
Z.___errno_location=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return dc.apply(null,arguments)};var ec=Z._emu_compute_audio_samples;
Z._emu_compute_audio_samples=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return ec.apply(null,arguments)};var fc=Z._emu_get_audio_buffer;
Z._emu_get_audio_buffer=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return fc.apply(null,arguments)};var hc=Z._emu_get_audio_buffer_length;
Z._emu_get_audio_buffer_length=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return hc.apply(null,arguments)};var ic=Z._emu_get_current_position;
Z._emu_get_current_position=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return ic.apply(null,arguments)};var jc=Z._emu_get_max_position;
Z._emu_get_max_position=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return jc.apply(null,arguments)};var kc=Z._emu_get_sample_rate;
Z._emu_get_sample_rate=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return kc.apply(null,arguments)};var lc=Z._emu_get_trace_streams;
Z._emu_get_trace_streams=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return lc.apply(null,arguments)};var mc=Z._emu_get_track_info;
Z._emu_get_track_info=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return mc.apply(null,arguments)};var nc=Z._emu_load_file;
Z._emu_load_file=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return nc.apply(null,arguments)};var oc=Z._emu_number_trace_streams;
Z._emu_number_trace_streams=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return oc.apply(null,arguments)};var pc=Z._emu_seek_position;
Z._emu_seek_position=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return pc.apply(null,arguments)};var qc=Z._emu_set_subsong;
Z._emu_set_subsong=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return qc.apply(null,arguments)};var rc=Z._emu_teardown;
Z._emu_teardown=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return rc.apply(null,arguments)};var sc=Z._fflush;Z._fflush=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return sc.apply(null,arguments)};
var tc=Z._free;Z._free=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return tc.apply(null,arguments)};var uc=Z._llvm_round_f64;
Z._llvm_round_f64=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return uc.apply(null,arguments)};var vc=Z._malloc;Z._malloc=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return vc.apply(null,arguments)};
var wc=Z._sbrk;Z._sbrk=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return wc.apply(null,arguments)};var xc=Z.establishStackSpace;
Z.establishStackSpace=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return xc.apply(null,arguments)};var yc=Z.getTempRet0;
Z.getTempRet0=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return yc.apply(null,arguments)};var zc=Z.setTempRet0;Z.setTempRet0=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return zc.apply(null,arguments)};
var Ac=Z.setThrew;Z.setThrew=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return Ac.apply(null,arguments)};var Bc=Z.stackAlloc;
Z.stackAlloc=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return Bc.apply(null,arguments)};var Cc=Z.stackRestore;Z.stackRestore=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return Cc.apply(null,arguments)};
var Dc=Z.stackSave;Z.stackSave=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return Dc.apply(null,arguments)};d.asm=Z;
d.___errno_location=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.___errno_location.apply(null,arguments)};
d._emu_compute_audio_samples=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_compute_audio_samples.apply(null,arguments)};
d._emu_get_audio_buffer=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_audio_buffer.apply(null,arguments)};
d._emu_get_audio_buffer_length=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_audio_buffer_length.apply(null,arguments)};
d._emu_get_current_position=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_current_position.apply(null,arguments)};
d._emu_get_max_position=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_max_position.apply(null,arguments)};
d._emu_get_sample_rate=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_sample_rate.apply(null,arguments)};
d._emu_get_trace_streams=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_trace_streams.apply(null,arguments)};
d._emu_get_track_info=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_get_track_info.apply(null,arguments)};
d._emu_load_file=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_load_file.apply(null,arguments)};
d._emu_number_trace_streams=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_number_trace_streams.apply(null,arguments)};
d._emu_seek_position=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_seek_position.apply(null,arguments)};
d._emu_set_subsong=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_set_subsong.apply(null,arguments)};
d._emu_teardown=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._emu_teardown.apply(null,arguments)};
d._fflush=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._fflush.apply(null,arguments)};d._free=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._free.apply(null,arguments)};
d._llvm_round_f64=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._llvm_round_f64.apply(null,arguments)};
var pb=d._malloc=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._malloc.apply(null,arguments)};d._sbrk=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm._sbrk.apply(null,arguments)};
d.establishStackSpace=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.establishStackSpace.apply(null,arguments)};
d.getTempRet0=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.getTempRet0.apply(null,arguments)};
d.setTempRet0=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.setTempRet0.apply(null,arguments)};
d.setThrew=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.setThrew.apply(null,arguments)};
var ja=d.stackAlloc=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.stackAlloc.apply(null,arguments)},ia=d.stackRestore=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.stackRestore.apply(null,
arguments)},ha=d.stackSave=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.stackSave.apply(null,arguments)};
d.dynCall_v=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.dynCall_v.apply(null,arguments)};
d.dynCall_vi=function(){assert(E,"you need to wait for the runtime to be ready (e.g. wait for main() to be called)");assert(!0,"the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");return d.asm.dynCall_vi.apply(null,arguments)};d.asm=Z;d.intArrayFromString||(d.intArrayFromString=function(){t("'intArrayFromString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.intArrayToString||(d.intArrayToString=function(){t("'intArrayToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.ccall=function(a,b,c,e){var f=d["_"+a];assert(f,"Cannot call unknown function "+a+", make sure it is exported");var g=[];a=0;assert("array"!==b,'Return type should not be "array".');if(e)for(var h=0;h<e.length;h++){var r=ta[c[h]];r?(0===a&&(a=ha()),g[h]=r(e[h])):g[h]=e[h]}c=f.apply(null,g);c="string"===b?va(c):"boolean"===b?!!c:c;0!==a&&ia(a);return c};d.cwrap||(d.cwrap=function(){t("'cwrap' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.setValue||(d.setValue=function(){t("'setValue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.getValue||(d.getValue=function(){t("'getValue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.allocate||(d.allocate=function(){t("'allocate' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.getMemory=function(a){if(la)if(E)var b=pb(a);else{assert(D);b=A[D>>2];a=b+a+15&-16;A[D>>2]=a;if(a=a>=y)Ia(),a=!0;a&&(A[D>>2]=b,b=0)}else b=ka(a);return b};d.Pointer_stringify=va;d.AsciiToString||(d.AsciiToString=function(){t("'AsciiToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.stringToAscii||(d.stringToAscii=function(){t("'stringToAscii' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.UTF8ArrayToString||(d.UTF8ArrayToString=function(){t("'UTF8ArrayToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.UTF8ToString=wa;d.stringToUTF8Array||(d.stringToUTF8Array=function(){t("'stringToUTF8Array' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.stringToUTF8||(d.stringToUTF8=function(){t("'stringToUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.lengthBytesUTF8||(d.lengthBytesUTF8=function(){t("'lengthBytesUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.UTF16ToString||(d.UTF16ToString=function(){t("'UTF16ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.stringToUTF16||(d.stringToUTF16=function(){t("'stringToUTF16' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.lengthBytesUTF16||(d.lengthBytesUTF16=function(){t("'lengthBytesUTF16' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.UTF32ToString||(d.UTF32ToString=function(){t("'UTF32ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.stringToUTF32||(d.stringToUTF32=function(){t("'stringToUTF32' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.lengthBytesUTF32||(d.lengthBytesUTF32=function(){t("'lengthBytesUTF32' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.allocateUTF8||(d.allocateUTF8=function(){t("'allocateUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.stackTrace||(d.stackTrace=function(){t("'stackTrace' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.addOnPreRun||(d.addOnPreRun=function(){t("'addOnPreRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.addOnInit||(d.addOnInit=function(){t("'addOnInit' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.addOnPreMain||(d.addOnPreMain=function(){t("'addOnPreMain' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.addOnExit||(d.addOnExit=function(){t("'addOnExit' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.addOnPostRun||(d.addOnPostRun=function(){t("'addOnPostRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.writeStringToMemory||(d.writeStringToMemory=function(){t("'writeStringToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.writeArrayToMemory||(d.writeArrayToMemory=function(){t("'writeArrayToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.writeAsciiToMemory||(d.writeAsciiToMemory=function(){t("'writeAsciiToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.addRunDependency=Ua;d.removeRunDependency=Va;d.ENV||(d.ENV=function(){t("'ENV' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.FS||(d.FS=function(){t("'FS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.FS_createFolder=Vb;d.FS_createPath=Wb;d.FS_createDataFile=Yb;
d.FS_createPreloadedFile=bc;d.FS_createLazyFile=ac;d.FS_createLink=Zb;d.FS_createDevice=W;d.FS_unlink=Kb;d.GL||(d.GL=function(){t("'GL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.staticAlloc||(d.staticAlloc=function(){t("'staticAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.dynamicAlloc||(d.dynamicAlloc=function(){t("'dynamicAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.warnOnce||(d.warnOnce=function(){t("'warnOnce' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.loadDynamicLibrary||(d.loadDynamicLibrary=function(){t("'loadDynamicLibrary' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.loadWebAssemblyModule||(d.loadWebAssemblyModule=function(){t("'loadWebAssemblyModule' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.getLEB||(d.getLEB=function(){t("'getLEB' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.getFunctionTables||(d.getFunctionTables=function(){t("'getFunctionTables' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.alignFunctionTables||(d.alignFunctionTables=function(){t("'alignFunctionTables' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.registerFunctions||(d.registerFunctions=function(){t("'registerFunctions' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.addFunction||(d.addFunction=function(){t("'addFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.removeFunction||(d.removeFunction=function(){t("'removeFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.getFuncWrapper||(d.getFuncWrapper=function(){t("'getFuncWrapper' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.prettyPrint||(d.prettyPrint=function(){t("'prettyPrint' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.makeBigInt||(d.makeBigInt=function(){t("'makeBigInt' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.dynCall||(d.dynCall=function(){t("'dynCall' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.getCompilerSetting||(d.getCompilerSetting=function(){t("'getCompilerSetting' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.stackSave||(d.stackSave=function(){t("'stackSave' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.stackRestore||(d.stackRestore=function(){t("'stackRestore' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.stackAlloc||(d.stackAlloc=function(){t("'stackAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.establishStackSpace||(d.establishStackSpace=function(){t("'establishStackSpace' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.print||(d.print=function(){t("'print' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});d.printErr||(d.printErr=function(){t("'printErr' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")});
d.ALLOC_NORMAL||Object.defineProperty(d,"ALLOC_NORMAL",{get:function(){t("'ALLOC_NORMAL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")}});d.ALLOC_STACK||Object.defineProperty(d,"ALLOC_STACK",{get:function(){t("'ALLOC_STACK' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")}});d.ALLOC_STATIC||Object.defineProperty(d,"ALLOC_STATIC",{get:function(){t("'ALLOC_STATIC' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")}});
d.ALLOC_DYNAMIC||Object.defineProperty(d,"ALLOC_DYNAMIC",{get:function(){t("'ALLOC_DYNAMIC' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")}});d.ALLOC_NONE||Object.defineProperty(d,"ALLOC_NONE",{get:function(){t("'ALLOC_NONE' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)")}});Sa=function Ec(){d.calledRun||Fc();d.calledRun||(Sa=Ec)};
function Fc(){function a(){if(!d.calledRun&&(d.calledRun=!0,!pa)){Ha();E||(E=!0,Ka(Ma));Ha();Ka(Na);if(d.onRuntimeInitialized)d.onRuntimeInitialized();assert(!d._main,'compiled without a main, but one is present. if you added it from JS, use Module["onRuntimeInitialized"]');Ha();if(d.postRun)for("function"==typeof d.postRun&&(d.postRun=[d.postRun]);d.postRun.length;){var a=d.postRun.shift();Qa.unshift(a)}Ka(Qa)}}if(!(0<F)){assert(0==(C&3));B[(C>>2)-1]=34821223;B[(C>>2)-2]=2310721022;if(d.preRun)for("function"==
typeof d.preRun&&(d.preRun=[d.preRun]);d.preRun.length;)Ra();Ka(La);0<F||d.calledRun||(d.setStatus?(d.setStatus("Running..."),setTimeout(function(){setTimeout(function(){d.setStatus("")},1);a()},1)):a(),Ha())}}d.run=Fc;var Gc=[];function t(a){if(d.onAbort)d.onAbort(a);void 0!==a?(fa(a),u(a),a=JSON.stringify(a)):a="";pa=!0;var b="abort("+a+") at "+Aa()+"";Gc&&Gc.forEach(function(c){b=c(b,a)});throw b;}d.abort=t;if(d.preInit)for("function"==typeof d.preInit&&(d.preInit=[d.preInit]);0<d.preInit.length;)d.preInit.pop()();
d.noExitRuntime=!0;Fc();
  return {
	Module: Module,  // expose original Module
  };
})(window.spp_backend_state_AYUMI);
/* pako 0.2.8 nodeca/pako */
!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var t;t="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,t.pako=e()}}(function(){return function e(t,i,n){function a(o,s){if(!i[o]){if(!t[o]){var f="function"==typeof require&&require;if(!s&&f)return f(o,!0);if(r)return r(o,!0);var l=new Error("Cannot find module '"+o+"'");throw l.code="MODULE_NOT_FOUND",l}var d=i[o]={exports:{}};t[o][0].call(d.exports,function(e){var i=t[o][1][e];return a(i?i:e)},d,d.exports,e,t,i,n)}return i[o].exports}for(var r="function"==typeof require&&require,o=0;o<n.length;o++)a(n[o]);return a}({1:[function(e,t,i){"use strict";var n="undefined"!=typeof Uint8Array&&"undefined"!=typeof Uint16Array&&"undefined"!=typeof Int32Array;i.assign=function(e){for(var t=Array.prototype.slice.call(arguments,1);t.length;){var i=t.shift();if(i){if("object"!=typeof i)throw new TypeError(i+"must be non-object");for(var n in i)i.hasOwnProperty(n)&&(e[n]=i[n])}}return e},i.shrinkBuf=function(e,t){return e.length===t?e:e.subarray?e.subarray(0,t):(e.length=t,e)};var a={arraySet:function(e,t,i,n,a){if(t.subarray&&e.subarray)return void e.set(t.subarray(i,i+n),a);for(var r=0;n>r;r++)e[a+r]=t[i+r]},flattenChunks:function(e){var t,i,n,a,r,o;for(n=0,t=0,i=e.length;i>t;t++)n+=e[t].length;for(o=new Uint8Array(n),a=0,t=0,i=e.length;i>t;t++)r=e[t],o.set(r,a),a+=r.length;return o}},r={arraySet:function(e,t,i,n,a){for(var r=0;n>r;r++)e[a+r]=t[i+r]},flattenChunks:function(e){return[].concat.apply([],e)}};i.setTyped=function(e){e?(i.Buf8=Uint8Array,i.Buf16=Uint16Array,i.Buf32=Int32Array,i.assign(i,a)):(i.Buf8=Array,i.Buf16=Array,i.Buf32=Array,i.assign(i,r))},i.setTyped(n)},{}],2:[function(e,t,i){"use strict";function n(e,t){if(65537>t&&(e.subarray&&o||!e.subarray&&r))return String.fromCharCode.apply(null,a.shrinkBuf(e,t));for(var i="",n=0;t>n;n++)i+=String.fromCharCode(e[n]);return i}var a=e("./common"),r=!0,o=!0;try{String.fromCharCode.apply(null,[0])}catch(s){r=!1}try{String.fromCharCode.apply(null,new Uint8Array(1))}catch(s){o=!1}for(var f=new a.Buf8(256),l=0;256>l;l++)f[l]=l>=252?6:l>=248?5:l>=240?4:l>=224?3:l>=192?2:1;f[254]=f[254]=1,i.string2buf=function(e){var t,i,n,r,o,s=e.length,f=0;for(r=0;s>r;r++)i=e.charCodeAt(r),55296===(64512&i)&&s>r+1&&(n=e.charCodeAt(r+1),56320===(64512&n)&&(i=65536+(i-55296<<10)+(n-56320),r++)),f+=128>i?1:2048>i?2:65536>i?3:4;for(t=new a.Buf8(f),o=0,r=0;f>o;r++)i=e.charCodeAt(r),55296===(64512&i)&&s>r+1&&(n=e.charCodeAt(r+1),56320===(64512&n)&&(i=65536+(i-55296<<10)+(n-56320),r++)),128>i?t[o++]=i:2048>i?(t[o++]=192|i>>>6,t[o++]=128|63&i):65536>i?(t[o++]=224|i>>>12,t[o++]=128|i>>>6&63,t[o++]=128|63&i):(t[o++]=240|i>>>18,t[o++]=128|i>>>12&63,t[o++]=128|i>>>6&63,t[o++]=128|63&i);return t},i.buf2binstring=function(e){return n(e,e.length)},i.binstring2buf=function(e){for(var t=new a.Buf8(e.length),i=0,n=t.length;n>i;i++)t[i]=e.charCodeAt(i);return t},i.buf2string=function(e,t){var i,a,r,o,s=t||e.length,l=new Array(2*s);for(a=0,i=0;s>i;)if(r=e[i++],128>r)l[a++]=r;else if(o=f[r],o>4)l[a++]=65533,i+=o-1;else{for(r&=2===o?31:3===o?15:7;o>1&&s>i;)r=r<<6|63&e[i++],o--;o>1?l[a++]=65533:65536>r?l[a++]=r:(r-=65536,l[a++]=55296|r>>10&1023,l[a++]=56320|1023&r)}return n(l,a)},i.utf8border=function(e,t){var i;for(t=t||e.length,t>e.length&&(t=e.length),i=t-1;i>=0&&128===(192&e[i]);)i--;return 0>i?t:0===i?t:i+f[e[i]]>t?i:t}},{"./common":1}],3:[function(e,t,i){"use strict";function n(e,t,i,n){for(var a=65535&e|0,r=e>>>16&65535|0,o=0;0!==i;){o=i>2e3?2e3:i,i-=o;do a=a+t[n++]|0,r=r+a|0;while(--o);a%=65521,r%=65521}return a|r<<16|0}t.exports=n},{}],4:[function(e,t,i){t.exports={Z_NO_FLUSH:0,Z_PARTIAL_FLUSH:1,Z_SYNC_FLUSH:2,Z_FULL_FLUSH:3,Z_FINISH:4,Z_BLOCK:5,Z_TREES:6,Z_OK:0,Z_STREAM_END:1,Z_NEED_DICT:2,Z_ERRNO:-1,Z_STREAM_ERROR:-2,Z_DATA_ERROR:-3,Z_BUF_ERROR:-5,Z_NO_COMPRESSION:0,Z_BEST_SPEED:1,Z_BEST_COMPRESSION:9,Z_DEFAULT_COMPRESSION:-1,Z_FILTERED:1,Z_HUFFMAN_ONLY:2,Z_RLE:3,Z_FIXED:4,Z_DEFAULT_STRATEGY:0,Z_BINARY:0,Z_TEXT:1,Z_UNKNOWN:2,Z_DEFLATED:8}},{}],5:[function(e,t,i){"use strict";function n(){for(var e,t=[],i=0;256>i;i++){e=i;for(var n=0;8>n;n++)e=1&e?3988292384^e>>>1:e>>>1;t[i]=e}return t}function a(e,t,i,n){var a=r,o=n+i;e=-1^e;for(var s=n;o>s;s++)e=e>>>8^a[255&(e^t[s])];return-1^e}var r=n();t.exports=a},{}],6:[function(e,t,i){"use strict";function n(){this.text=0,this.time=0,this.xflags=0,this.os=0,this.extra=null,this.extra_len=0,this.name="",this.comment="",this.hcrc=0,this.done=!1}t.exports=n},{}],7:[function(e,t,i){"use strict";var n=30,a=12;t.exports=function(e,t){var i,r,o,s,f,l,d,u,h,c,b,w,m,k,_,g,v,p,x,y,S,E,B,Z,A;i=e.state,r=e.next_in,Z=e.input,o=r+(e.avail_in-5),s=e.next_out,A=e.output,f=s-(t-e.avail_out),l=s+(e.avail_out-257),d=i.dmax,u=i.wsize,h=i.whave,c=i.wnext,b=i.window,w=i.hold,m=i.bits,k=i.lencode,_=i.distcode,g=(1<<i.lenbits)-1,v=(1<<i.distbits)-1;e:do{15>m&&(w+=Z[r++]<<m,m+=8,w+=Z[r++]<<m,m+=8),p=k[w&g];t:for(;;){if(x=p>>>24,w>>>=x,m-=x,x=p>>>16&255,0===x)A[s++]=65535&p;else{if(!(16&x)){if(0===(64&x)){p=k[(65535&p)+(w&(1<<x)-1)];continue t}if(32&x){i.mode=a;break e}e.msg="invalid literal/length code",i.mode=n;break e}y=65535&p,x&=15,x&&(x>m&&(w+=Z[r++]<<m,m+=8),y+=w&(1<<x)-1,w>>>=x,m-=x),15>m&&(w+=Z[r++]<<m,m+=8,w+=Z[r++]<<m,m+=8),p=_[w&v];i:for(;;){if(x=p>>>24,w>>>=x,m-=x,x=p>>>16&255,!(16&x)){if(0===(64&x)){p=_[(65535&p)+(w&(1<<x)-1)];continue i}e.msg="invalid distance code",i.mode=n;break e}if(S=65535&p,x&=15,x>m&&(w+=Z[r++]<<m,m+=8,x>m&&(w+=Z[r++]<<m,m+=8)),S+=w&(1<<x)-1,S>d){e.msg="invalid distance too far back",i.mode=n;break e}if(w>>>=x,m-=x,x=s-f,S>x){if(x=S-x,x>h&&i.sane){e.msg="invalid distance too far back",i.mode=n;break e}if(E=0,B=b,0===c){if(E+=u-x,y>x){y-=x;do A[s++]=b[E++];while(--x);E=s-S,B=A}}else if(x>c){if(E+=u+c-x,x-=c,y>x){y-=x;do A[s++]=b[E++];while(--x);if(E=0,y>c){x=c,y-=x;do A[s++]=b[E++];while(--x);E=s-S,B=A}}}else if(E+=c-x,y>x){y-=x;do A[s++]=b[E++];while(--x);E=s-S,B=A}for(;y>2;)A[s++]=B[E++],A[s++]=B[E++],A[s++]=B[E++],y-=3;y&&(A[s++]=B[E++],y>1&&(A[s++]=B[E++]))}else{E=s-S;do A[s++]=A[E++],A[s++]=A[E++],A[s++]=A[E++],y-=3;while(y>2);y&&(A[s++]=A[E++],y>1&&(A[s++]=A[E++]))}break}}break}}while(o>r&&l>s);y=m>>3,r-=y,m-=y<<3,w&=(1<<m)-1,e.next_in=r,e.next_out=s,e.avail_in=o>r?5+(o-r):5-(r-o),e.avail_out=l>s?257+(l-s):257-(s-l),i.hold=w,i.bits=m}},{}],8:[function(e,t,i){"use strict";function n(e){return(e>>>24&255)+(e>>>8&65280)+((65280&e)<<8)+((255&e)<<24)}function a(){this.mode=0,this.last=!1,this.wrap=0,this.havedict=!1,this.flags=0,this.dmax=0,this.check=0,this.total=0,this.head=null,this.wbits=0,this.wsize=0,this.whave=0,this.wnext=0,this.window=null,this.hold=0,this.bits=0,this.length=0,this.offset=0,this.extra=0,this.lencode=null,this.distcode=null,this.lenbits=0,this.distbits=0,this.ncode=0,this.nlen=0,this.ndist=0,this.have=0,this.next=null,this.lens=new k.Buf16(320),this.work=new k.Buf16(288),this.lendyn=null,this.distdyn=null,this.sane=0,this.back=0,this.was=0}function r(e){var t;return e&&e.state?(t=e.state,e.total_in=e.total_out=t.total=0,e.msg="",t.wrap&&(e.adler=1&t.wrap),t.mode=T,t.last=0,t.havedict=0,t.dmax=32768,t.head=null,t.hold=0,t.bits=0,t.lencode=t.lendyn=new k.Buf32(be),t.distcode=t.distdyn=new k.Buf32(we),t.sane=1,t.back=-1,A):N}function o(e){var t;return e&&e.state?(t=e.state,t.wsize=0,t.whave=0,t.wnext=0,r(e)):N}function s(e,t){var i,n;return e&&e.state?(n=e.state,0>t?(i=0,t=-t):(i=(t>>4)+1,48>t&&(t&=15)),t&&(8>t||t>15)?N:(null!==n.window&&n.wbits!==t&&(n.window=null),n.wrap=i,n.wbits=t,o(e))):N}function f(e,t){var i,n;return e?(n=new a,e.state=n,n.window=null,i=s(e,t),i!==A&&(e.state=null),i):N}function l(e){return f(e,ke)}function d(e){if(_e){var t;for(w=new k.Buf32(512),m=new k.Buf32(32),t=0;144>t;)e.lens[t++]=8;for(;256>t;)e.lens[t++]=9;for(;280>t;)e.lens[t++]=7;for(;288>t;)e.lens[t++]=8;for(p(y,e.lens,0,288,w,0,e.work,{bits:9}),t=0;32>t;)e.lens[t++]=5;p(S,e.lens,0,32,m,0,e.work,{bits:5}),_e=!1}e.lencode=w,e.lenbits=9,e.distcode=m,e.distbits=5}function u(e,t,i,n){var a,r=e.state;return null===r.window&&(r.wsize=1<<r.wbits,r.wnext=0,r.whave=0,r.window=new k.Buf8(r.wsize)),n>=r.wsize?(k.arraySet(r.window,t,i-r.wsize,r.wsize,0),r.wnext=0,r.whave=r.wsize):(a=r.wsize-r.wnext,a>n&&(a=n),k.arraySet(r.window,t,i-n,a,r.wnext),n-=a,n?(k.arraySet(r.window,t,i-n,n,0),r.wnext=n,r.whave=r.wsize):(r.wnext+=a,r.wnext===r.wsize&&(r.wnext=0),r.whave<r.wsize&&(r.whave+=a))),0}function h(e,t){var i,a,r,o,s,f,l,h,c,b,w,m,be,we,me,ke,_e,ge,ve,pe,xe,ye,Se,Ee,Be=0,Ze=new k.Buf8(4),Ae=[16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15];if(!e||!e.state||!e.output||!e.input&&0!==e.avail_in)return N;i=e.state,i.mode===G&&(i.mode=X),s=e.next_out,r=e.output,l=e.avail_out,o=e.next_in,a=e.input,f=e.avail_in,h=i.hold,c=i.bits,b=f,w=l,ye=A;e:for(;;)switch(i.mode){case T:if(0===i.wrap){i.mode=X;break}for(;16>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(2&i.wrap&&35615===h){i.check=0,Ze[0]=255&h,Ze[1]=h>>>8&255,i.check=g(i.check,Ze,2,0),h=0,c=0,i.mode=U;break}if(i.flags=0,i.head&&(i.head.done=!1),!(1&i.wrap)||(((255&h)<<8)+(h>>8))%31){e.msg="incorrect header check",i.mode=ue;break}if((15&h)!==F){e.msg="unknown compression method",i.mode=ue;break}if(h>>>=4,c-=4,xe=(15&h)+8,0===i.wbits)i.wbits=xe;else if(xe>i.wbits){e.msg="invalid window size",i.mode=ue;break}i.dmax=1<<xe,e.adler=i.check=1,i.mode=512&h?Y:G,h=0,c=0;break;case U:for(;16>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(i.flags=h,(255&i.flags)!==F){e.msg="unknown compression method",i.mode=ue;break}if(57344&i.flags){e.msg="unknown header flags set",i.mode=ue;break}i.head&&(i.head.text=h>>8&1),512&i.flags&&(Ze[0]=255&h,Ze[1]=h>>>8&255,i.check=g(i.check,Ze,2,0)),h=0,c=0,i.mode=D;case D:for(;32>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}i.head&&(i.head.time=h),512&i.flags&&(Ze[0]=255&h,Ze[1]=h>>>8&255,Ze[2]=h>>>16&255,Ze[3]=h>>>24&255,i.check=g(i.check,Ze,4,0)),h=0,c=0,i.mode=L;case L:for(;16>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}i.head&&(i.head.xflags=255&h,i.head.os=h>>8),512&i.flags&&(Ze[0]=255&h,Ze[1]=h>>>8&255,i.check=g(i.check,Ze,2,0)),h=0,c=0,i.mode=H;case H:if(1024&i.flags){for(;16>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}i.length=h,i.head&&(i.head.extra_len=h),512&i.flags&&(Ze[0]=255&h,Ze[1]=h>>>8&255,i.check=g(i.check,Ze,2,0)),h=0,c=0}else i.head&&(i.head.extra=null);i.mode=j;case j:if(1024&i.flags&&(m=i.length,m>f&&(m=f),m&&(i.head&&(xe=i.head.extra_len-i.length,i.head.extra||(i.head.extra=new Array(i.head.extra_len)),k.arraySet(i.head.extra,a,o,m,xe)),512&i.flags&&(i.check=g(i.check,a,m,o)),f-=m,o+=m,i.length-=m),i.length))break e;i.length=0,i.mode=M;case M:if(2048&i.flags){if(0===f)break e;m=0;do xe=a[o+m++],i.head&&xe&&i.length<65536&&(i.head.name+=String.fromCharCode(xe));while(xe&&f>m);if(512&i.flags&&(i.check=g(i.check,a,m,o)),f-=m,o+=m,xe)break e}else i.head&&(i.head.name=null);i.length=0,i.mode=K;case K:if(4096&i.flags){if(0===f)break e;m=0;do xe=a[o+m++],i.head&&xe&&i.length<65536&&(i.head.comment+=String.fromCharCode(xe));while(xe&&f>m);if(512&i.flags&&(i.check=g(i.check,a,m,o)),f-=m,o+=m,xe)break e}else i.head&&(i.head.comment=null);i.mode=P;case P:if(512&i.flags){for(;16>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(h!==(65535&i.check)){e.msg="header crc mismatch",i.mode=ue;break}h=0,c=0}i.head&&(i.head.hcrc=i.flags>>9&1,i.head.done=!0),e.adler=i.check=0,i.mode=G;break;case Y:for(;32>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}e.adler=i.check=n(h),h=0,c=0,i.mode=q;case q:if(0===i.havedict)return e.next_out=s,e.avail_out=l,e.next_in=o,e.avail_in=f,i.hold=h,i.bits=c,R;e.adler=i.check=1,i.mode=G;case G:if(t===B||t===Z)break e;case X:if(i.last){h>>>=7&c,c-=7&c,i.mode=fe;break}for(;3>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}switch(i.last=1&h,h>>>=1,c-=1,3&h){case 0:i.mode=W;break;case 1:if(d(i),i.mode=te,t===Z){h>>>=2,c-=2;break e}break;case 2:i.mode=V;break;case 3:e.msg="invalid block type",i.mode=ue}h>>>=2,c-=2;break;case W:for(h>>>=7&c,c-=7&c;32>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if((65535&h)!==(h>>>16^65535)){e.msg="invalid stored block lengths",i.mode=ue;break}if(i.length=65535&h,h=0,c=0,i.mode=J,t===Z)break e;case J:i.mode=Q;case Q:if(m=i.length){if(m>f&&(m=f),m>l&&(m=l),0===m)break e;k.arraySet(r,a,o,m,s),f-=m,o+=m,l-=m,s+=m,i.length-=m;break}i.mode=G;break;case V:for(;14>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(i.nlen=(31&h)+257,h>>>=5,c-=5,i.ndist=(31&h)+1,h>>>=5,c-=5,i.ncode=(15&h)+4,h>>>=4,c-=4,i.nlen>286||i.ndist>30){e.msg="too many length or distance symbols",i.mode=ue;break}i.have=0,i.mode=$;case $:for(;i.have<i.ncode;){for(;3>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}i.lens[Ae[i.have++]]=7&h,h>>>=3,c-=3}for(;i.have<19;)i.lens[Ae[i.have++]]=0;if(i.lencode=i.lendyn,i.lenbits=7,Se={bits:i.lenbits},ye=p(x,i.lens,0,19,i.lencode,0,i.work,Se),i.lenbits=Se.bits,ye){e.msg="invalid code lengths set",i.mode=ue;break}i.have=0,i.mode=ee;case ee:for(;i.have<i.nlen+i.ndist;){for(;Be=i.lencode[h&(1<<i.lenbits)-1],me=Be>>>24,ke=Be>>>16&255,_e=65535&Be,!(c>=me);){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(16>_e)h>>>=me,c-=me,i.lens[i.have++]=_e;else{if(16===_e){for(Ee=me+2;Ee>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(h>>>=me,c-=me,0===i.have){e.msg="invalid bit length repeat",i.mode=ue;break}xe=i.lens[i.have-1],m=3+(3&h),h>>>=2,c-=2}else if(17===_e){for(Ee=me+3;Ee>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}h>>>=me,c-=me,xe=0,m=3+(7&h),h>>>=3,c-=3}else{for(Ee=me+7;Ee>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}h>>>=me,c-=me,xe=0,m=11+(127&h),h>>>=7,c-=7}if(i.have+m>i.nlen+i.ndist){e.msg="invalid bit length repeat",i.mode=ue;break}for(;m--;)i.lens[i.have++]=xe}}if(i.mode===ue)break;if(0===i.lens[256]){e.msg="invalid code -- missing end-of-block",i.mode=ue;break}if(i.lenbits=9,Se={bits:i.lenbits},ye=p(y,i.lens,0,i.nlen,i.lencode,0,i.work,Se),i.lenbits=Se.bits,ye){e.msg="invalid literal/lengths set",i.mode=ue;break}if(i.distbits=6,i.distcode=i.distdyn,Se={bits:i.distbits},ye=p(S,i.lens,i.nlen,i.ndist,i.distcode,0,i.work,Se),i.distbits=Se.bits,ye){e.msg="invalid distances set",i.mode=ue;break}if(i.mode=te,t===Z)break e;case te:i.mode=ie;case ie:if(f>=6&&l>=258){e.next_out=s,e.avail_out=l,e.next_in=o,e.avail_in=f,i.hold=h,i.bits=c,v(e,w),s=e.next_out,r=e.output,l=e.avail_out,o=e.next_in,a=e.input,f=e.avail_in,h=i.hold,c=i.bits,i.mode===G&&(i.back=-1);break}for(i.back=0;Be=i.lencode[h&(1<<i.lenbits)-1],me=Be>>>24,ke=Be>>>16&255,_e=65535&Be,!(c>=me);){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(ke&&0===(240&ke)){for(ge=me,ve=ke,pe=_e;Be=i.lencode[pe+((h&(1<<ge+ve)-1)>>ge)],me=Be>>>24,ke=Be>>>16&255,_e=65535&Be,!(c>=ge+me);){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}h>>>=ge,c-=ge,i.back+=ge}if(h>>>=me,c-=me,i.back+=me,i.length=_e,0===ke){i.mode=se;break}if(32&ke){i.back=-1,i.mode=G;break}if(64&ke){e.msg="invalid literal/length code",i.mode=ue;break}i.extra=15&ke,i.mode=ne;case ne:if(i.extra){for(Ee=i.extra;Ee>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}i.length+=h&(1<<i.extra)-1,h>>>=i.extra,c-=i.extra,i.back+=i.extra}i.was=i.length,i.mode=ae;case ae:for(;Be=i.distcode[h&(1<<i.distbits)-1],me=Be>>>24,ke=Be>>>16&255,_e=65535&Be,!(c>=me);){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(0===(240&ke)){for(ge=me,ve=ke,pe=_e;Be=i.distcode[pe+((h&(1<<ge+ve)-1)>>ge)],me=Be>>>24,ke=Be>>>16&255,_e=65535&Be,!(c>=ge+me);){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}h>>>=ge,c-=ge,i.back+=ge}if(h>>>=me,c-=me,i.back+=me,64&ke){e.msg="invalid distance code",i.mode=ue;break}i.offset=_e,i.extra=15&ke,i.mode=re;case re:if(i.extra){for(Ee=i.extra;Ee>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}i.offset+=h&(1<<i.extra)-1,h>>>=i.extra,c-=i.extra,i.back+=i.extra}if(i.offset>i.dmax){e.msg="invalid distance too far back",i.mode=ue;break}i.mode=oe;case oe:if(0===l)break e;if(m=w-l,i.offset>m){if(m=i.offset-m,m>i.whave&&i.sane){e.msg="invalid distance too far back",i.mode=ue;break}m>i.wnext?(m-=i.wnext,be=i.wsize-m):be=i.wnext-m,m>i.length&&(m=i.length),we=i.window}else we=r,be=s-i.offset,m=i.length;m>l&&(m=l),l-=m,i.length-=m;do r[s++]=we[be++];while(--m);0===i.length&&(i.mode=ie);break;case se:if(0===l)break e;r[s++]=i.length,l--,i.mode=ie;break;case fe:if(i.wrap){for(;32>c;){if(0===f)break e;f--,h|=a[o++]<<c,c+=8}if(w-=l,e.total_out+=w,i.total+=w,w&&(e.adler=i.check=i.flags?g(i.check,r,w,s-w):_(i.check,r,w,s-w)),w=l,(i.flags?h:n(h))!==i.check){e.msg="incorrect data check",i.mode=ue;break}h=0,c=0}i.mode=le;case le:if(i.wrap&&i.flags){for(;32>c;){if(0===f)break e;f--,h+=a[o++]<<c,c+=8}if(h!==(4294967295&i.total)){e.msg="incorrect length check",i.mode=ue;break}h=0,c=0}i.mode=de;case de:ye=z;break e;case ue:ye=O;break e;case he:return C;case ce:default:return N}return e.next_out=s,e.avail_out=l,e.next_in=o,e.avail_in=f,i.hold=h,i.bits=c,(i.wsize||w!==e.avail_out&&i.mode<ue&&(i.mode<fe||t!==E))&&u(e,e.output,e.next_out,w-e.avail_out)?(i.mode=he,C):(b-=e.avail_in,w-=e.avail_out,e.total_in+=b,e.total_out+=w,i.total+=w,i.wrap&&w&&(e.adler=i.check=i.flags?g(i.check,r,w,e.next_out-w):_(i.check,r,w,e.next_out-w)),e.data_type=i.bits+(i.last?64:0)+(i.mode===G?128:0)+(i.mode===te||i.mode===J?256:0),(0===b&&0===w||t===E)&&ye===A&&(ye=I),ye)}function c(e){if(!e||!e.state)return N;var t=e.state;return t.window&&(t.window=null),e.state=null,A}function b(e,t){var i;return e&&e.state?(i=e.state,0===(2&i.wrap)?N:(i.head=t,t.done=!1,A)):N}var w,m,k=e("../utils/common"),_=e("./adler32"),g=e("./crc32"),v=e("./inffast"),p=e("./inftrees"),x=0,y=1,S=2,E=4,B=5,Z=6,A=0,z=1,R=2,N=-2,O=-3,C=-4,I=-5,F=8,T=1,U=2,D=3,L=4,H=5,j=6,M=7,K=8,P=9,Y=10,q=11,G=12,X=13,W=14,J=15,Q=16,V=17,$=18,ee=19,te=20,ie=21,ne=22,ae=23,re=24,oe=25,se=26,fe=27,le=28,de=29,ue=30,he=31,ce=32,be=852,we=592,me=15,ke=me,_e=!0;i.inflateReset=o,i.inflateReset2=s,i.inflateResetKeep=r,i.inflateInit=l,i.inflateInit2=f,i.inflate=h,i.inflateEnd=c,i.inflateGetHeader=b,i.inflateInfo="pako inflate (from Nodeca project)"},{"../utils/common":1,"./adler32":3,"./crc32":5,"./inffast":7,"./inftrees":9}],9:[function(e,t,i){"use strict";var n=e("../utils/common"),a=15,r=852,o=592,s=0,f=1,l=2,d=[3,4,5,6,7,8,9,10,11,13,15,17,19,23,27,31,35,43,51,59,67,83,99,115,131,163,195,227,258,0,0],u=[16,16,16,16,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21,21,21,16,72,78],h=[1,2,3,4,5,7,9,13,17,25,33,49,65,97,129,193,257,385,513,769,1025,1537,2049,3073,4097,6145,8193,12289,16385,24577,0,0],c=[16,16,16,16,17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,25,25,26,26,27,27,28,28,29,29,64,64];t.exports=function(e,t,i,b,w,m,k,_){var g,v,p,x,y,S,E,B,Z,A=_.bits,z=0,R=0,N=0,O=0,C=0,I=0,F=0,T=0,U=0,D=0,L=null,H=0,j=new n.Buf16(a+1),M=new n.Buf16(a+1),K=null,P=0;for(z=0;a>=z;z++)j[z]=0;for(R=0;b>R;R++)j[t[i+R]]++;for(C=A,O=a;O>=1&&0===j[O];O--);if(C>O&&(C=O),0===O)return w[m++]=20971520,w[m++]=20971520,_.bits=1,0;for(N=1;O>N&&0===j[N];N++);for(N>C&&(C=N),T=1,z=1;a>=z;z++)if(T<<=1,T-=j[z],0>T)return-1;if(T>0&&(e===s||1!==O))return-1;for(M[1]=0,z=1;a>z;z++)M[z+1]=M[z]+j[z];for(R=0;b>R;R++)0!==t[i+R]&&(k[M[t[i+R]]++]=R);if(e===s?(L=K=k,S=19):e===f?(L=d,H-=257,K=u,P-=257,S=256):(L=h,K=c,S=-1),D=0,R=0,z=N,y=m,I=C,F=0,p=-1,U=1<<C,x=U-1,e===f&&U>r||e===l&&U>o)return 1;for(var Y=0;;){Y++,E=z-F,k[R]<S?(B=0,Z=k[R]):k[R]>S?(B=K[P+k[R]],Z=L[H+k[R]]):(B=96,Z=0),g=1<<z-F,v=1<<I,N=v;do v-=g,w[y+(D>>F)+v]=E<<24|B<<16|Z|0;while(0!==v);for(g=1<<z-1;D&g;)g>>=1;if(0!==g?(D&=g-1,D+=g):D=0,R++,0===--j[z]){if(z===O)break;z=t[i+k[R]]}if(z>C&&(D&x)!==p){for(0===F&&(F=C),y+=N,I=z-F,T=1<<I;O>I+F&&(T-=j[I+F],!(0>=T));)I++,T<<=1;if(U+=1<<I,e===f&&U>r||e===l&&U>o)return 1;p=D&x,w[p]=C<<24|I<<16|y-m|0}}return 0!==D&&(w[y+D]=z-F<<24|64<<16|0),_.bits=C,0}},{"../utils/common":1}],10:[function(e,t,i){"use strict";t.exports={2:"need dictionary",1:"stream end",0:"","-1":"file error","-2":"stream error","-3":"data error","-4":"insufficient memory","-5":"buffer error","-6":"incompatible version"}},{}],11:[function(e,t,i){"use strict";function n(){this.input=null,this.next_in=0,this.avail_in=0,this.total_in=0,this.output=null,this.next_out=0,this.avail_out=0,this.total_out=0,this.msg="",this.state=null,this.data_type=2,this.adler=0}t.exports=n},{}],"/lib/inflate.js":[function(e,t,i){"use strict";function n(e,t){var i=new c(t);if(i.push(e,!0),i.err)throw i.msg;return i.result}function a(e,t){return t=t||{},t.raw=!0,n(e,t)}var r=e("./zlib/inflate.js"),o=e("./utils/common"),s=e("./utils/strings"),f=e("./zlib/constants"),l=e("./zlib/messages"),d=e("./zlib/zstream"),u=e("./zlib/gzheader"),h=Object.prototype.toString,c=function(e){this.options=o.assign({chunkSize:16384,windowBits:0,to:""},e||{});var t=this.options;t.raw&&t.windowBits>=0&&t.windowBits<16&&(t.windowBits=-t.windowBits,0===t.windowBits&&(t.windowBits=-15)),!(t.windowBits>=0&&t.windowBits<16)||e&&e.windowBits||(t.windowBits+=32),t.windowBits>15&&t.windowBits<48&&0===(15&t.windowBits)&&(t.windowBits|=15),this.err=0,this.msg="",this.ended=!1,this.chunks=[],this.strm=new d,this.strm.avail_out=0;var i=r.inflateInit2(this.strm,t.windowBits);if(i!==f.Z_OK)throw new Error(l[i]);this.header=new u,r.inflateGetHeader(this.strm,this.header)};c.prototype.push=function(e,t){var i,n,a,l,d,u=this.strm,c=this.options.chunkSize,b=!1;if(this.ended)return!1;n=t===~~t?t:t===!0?f.Z_FINISH:f.Z_NO_FLUSH,"string"==typeof e?u.input=s.binstring2buf(e):"[object ArrayBuffer]"===h.call(e)?u.input=new Uint8Array(e):u.input=e,u.next_in=0,u.avail_in=u.input.length;do{if(0===u.avail_out&&(u.output=new o.Buf8(c),u.next_out=0,u.avail_out=c),i=r.inflate(u,f.Z_NO_FLUSH),i===f.Z_BUF_ERROR&&b===!0&&(i=f.Z_OK,b=!1),i!==f.Z_STREAM_END&&i!==f.Z_OK)return this.onEnd(i),this.ended=!0,!1;u.next_out&&(0===u.avail_out||i===f.Z_STREAM_END||0===u.avail_in&&(n===f.Z_FINISH||n===f.Z_SYNC_FLUSH))&&("string"===this.options.to?(a=s.utf8border(u.output,u.next_out),l=u.next_out-a,d=s.buf2string(u.output,a),u.next_out=l,u.avail_out=c-l,l&&o.arraySet(u.output,u.output,a,l,0),this.onData(d)):this.onData(o.shrinkBuf(u.output,u.next_out))),0===u.avail_in&&0===u.avail_out&&(b=!0)}while((u.avail_in>0||0===u.avail_out)&&i!==f.Z_STREAM_END);return i===f.Z_STREAM_END&&(n=f.Z_FINISH),n===f.Z_FINISH?(i=r.inflateEnd(this.strm),this.onEnd(i),this.ended=!0,i===f.Z_OK):n===f.Z_SYNC_FLUSH?(this.onEnd(f.Z_OK),u.avail_out=0,!0):!0},c.prototype.onData=function(e){this.chunks.push(e)},c.prototype.onEnd=function(e){e===f.Z_OK&&("string"===this.options.to?this.result=this.chunks.join(""):this.result=o.flattenChunks(this.chunks)),this.chunks=[],this.err=e,this.msg=this.strm.msg},i.Inflate=c,i.inflate=n,i.inflateRaw=a,i.ungzip=n},{"./utils/common":1,"./utils/strings":2,"./zlib/constants":4,"./zlib/gzheader":6,"./zlib/inflate.js":8,"./zlib/messages":10,"./zlib/zstream":11}]},{},[])("/lib/inflate.js")});
// LH4, LHA (-lh4-) extractor, no crc/sum-checks. Will extract SFX-archives as well.
// Erland Ranvinge (erland.ranvinge@gmail.com)
// Based on a mix of Nobuyasu Suehiro's Java implementation and Simon Howard's C version.
// modified version: added 1st file extraction (without having to know its name)
var LhaArrayReader=function(e){this.buffer=e,this.offset=0,this.subOffset=7};LhaArrayReader.SeekAbsolute=0,LhaArrayReader.SeekRelative=1,LhaArrayReader.prototype.readBits=function(e){for(var t=[1,2,4,8,16,32,64,128],r=this.buffer[this.offset],a=0,s=0;s<e;s++){if(a<<=1,a|=(r&t[this.subOffset])>>this.subOffset,this.subOffset--,this.subOffset<0){if(this.offset+1>=this.buffer.length)return-1;r=this.buffer[++this.offset],this.subOffset=7}}return a},LhaArrayReader.prototype.readUInt8=function(){return this.offset+1>=this.buffer.length?-1:this.buffer[this.offset++]},LhaArrayReader.prototype.readUInt16=function(){if(this.offset+2>=this.buffer.length)return-1;var e=255&this.buffer[this.offset]|this.buffer[this.offset+1]<<8&65280;return this.offset+=2,e},LhaArrayReader.prototype.readUInt32=function(){if(this.offset+4>=this.buffer.length)return-1;var e=255&this.buffer[this.offset]|this.buffer[this.offset+1]<<8&65280|this.buffer[this.offset+2]<<16&16711680|this.buffer[this.offset+3]<<24&4278190080;return this.offset+=4,e},LhaArrayReader.prototype.readString=function(e){if(this.offset+e>=this.buffer.length)return-1;for(var t="",r=0;r<e;r++)t+=String.fromCharCode(this.buffer[this.offset++]);return t},LhaArrayReader.prototype.readLength=function(){var e=this.readBits(3);if(-1==e)return-1;if(7==e)for(;0!=this.readBits(1);)e++;return e},LhaArrayReader.prototype.seek=function(e,t){switch(t){case LhaArrayReader.SeekAbsolute:this.offset=e,this.subOffset=7;break;case LhaArrayReader.SeekRelative:this.offset+=e,this.subOffset=7}},LhaArrayReader.prototype.getPosition=function(){return this.offset};var LhaArrayWriter=function(e){this.offset=0,this.size=e,this.data=new Uint8Array(e)};LhaArrayWriter.prototype.write=function(e){this.data[this.offset++]=e};var LhaTree=function(){};LhaTree.LEAF=32768,LhaTree.prototype.setConstant=function(e){this.tree[0]=e|LhaTree.LEAF},LhaTree.prototype.expand=function(){for(var e=this.allocated;this.nextEntry<e;)this.tree[this.nextEntry]=this.allocated,this.allocated+=2,this.nextEntry++},LhaTree.prototype.addCodesWithLength=function(e,t){for(var r=!0,a=0;a<e.length;a++)if(e[a]==t){var s=this.nextEntry++;this.tree[s]=a|LhaTree.LEAF}else e[a]>t&&(r=!1);return r},LhaTree.prototype.build=function(e,t){this.tree=[];for(var r=0;r<t;r++)this.tree[r]=LhaTree.LEAF;this.nextEntry=0,this.allocated=1;for(var a=0;this.expand(),a++,!this.addCodesWithLength(e,a););},LhaTree.prototype.readCode=function(e){for(var t=this.tree[0];0==(t&LhaTree.LEAF);){var r=e.readBits(1);t=this.tree[t+r]}return t&~LhaTree.LEAF};var LhaRingBuffer=function(e){this.data=[],this.size=e,this.offset=0};LhaRingBuffer.prototype.add=function(e){this.data[this.offset]=e,this.offset=(this.offset+1)%this.size},LhaRingBuffer.prototype.get=function(e,t){for(var r=this.offset+this.size-e-1,a=[],s=0;s<t;s++){var i=this.data[(r+s)%this.size];a.push(i),this.add(i)}return a};var LhaReader=function(e){if(this.reader=e,this.offsetTree=new LhaTree,this.codeTree=new LhaTree,this.ringBuffer=new LhaRingBuffer(8192),this.entries={},"MZ"==e.readString(2)){var t=e.readUInt16(),r=512*(e.readUInt16()-1)+(0!=t?t:512);e.seek(r,LhaArrayReader.SeekAbsolute)}else e.seek(0,LhaArrayReader.SeekAbsolute);for(;;){var a={};if(a.size=e.readUInt8(),a.size<=0)break;a.checksum=e.readUInt8(),a.id=e.readString(5),a.packedSize=e.readUInt32(),a.originalSize=e.readUInt32(),a.datetime=e.readUInt32(),a.attributes=e.readUInt16();var s=e.readUInt8();a.filename=e.readString(s).toLowerCase(),a.crc=e.readUInt16(),a.offset=e.getPosition(),this.entries[a.filename]=a,e.seek(a.packedSize,LhaArrayReader.SeekRelative)}};LhaReader.prototype.readTempTable=function(){var e=this.reader,t=Math.min(e.readBits(5),19);if(t<=0){var r=e.readBits(5);this.offsetTree.setConstant(r)}else{for(var a=[],s=0;s<t;s++){var i=e.readLength();if(a.push(i),2==s)for(var f=e.readBits(2);0<f--;)a.push(0),s++}this.offsetTree.build(a,38)}},LhaReader.prototype.readCodeTable=function(){var e=this.reader,t=Math.min(e.readBits(9),510);if(t<=0){var r=e.readBits(9);this.codeTree.setConstant(r)}else{for(var a=[],s=0;s<t;){var i=this.offsetTree.readCode(e);if(i<=2){var f=1;for(1==i?f=e.readBits(4)+3:2==i&&(f=e.readBits(9)+20);0<=--f;)a.push(0),s++}else a.push(i-2),s++}this.codeTree.build(a,1020)}},LhaReader.prototype.readOffsetTable=function(){var e=this.reader,t=Math.min(e.readBits(4),14);if(t<=0){var r=e.readBits(4);this.offsetTree.setConstant(r)}else{for(var a=[],s=0;s<t;s++){var i=e.readLength();a[s]=i}this.offsetTree.build(a,38)}},LhaReader.prototype.extract=function(e,t,r){null==e&&(e=Object.keys(this.entries)[0]);var a=this.entries[e];if(!a)return null;this.reader.seek(a.offset,LhaArrayReader.SeekAbsolute);var s=new LhaArrayWriter(a.originalSize),i=this;return function e(){if(i.extractBlock(s)){if(t&&t(s.offset,s.size),s.offset>=s.size)return;setTimeout(e,1)}}(),s.data},LhaReader.prototype.extractBlock=function(e){var t=this.reader,r=t.readBits(16);if(r<=0||t.offset>=t.size)return!1;this.readTempTable(),this.readCodeTable(),this.readOffsetTable();for(var a=0;a<r;a++){var s=this.codeTree.readCode(t);if(s<256)this.ringBuffer.add(s),e.write(s);else{var i=this.offsetTree.readCode(t),f=i;if(2<=i){f=t.readBits(i-1);f+=1<<i-1}var h=s-256+3,o=this.ringBuffer.get(f,h);for(var n in o)e.write(o[n])}}return!0};/*
 ayumi_adapter.js: Adapts ayumi backend to generic WebAudio/ScriptProcessor player.

   version 1.1
   copyright (C) 2018-2023 Juergen Wothke

*/

class AyumiConverterBase {
	constructor()
	{
		this._ptr = 0;
	}

	getIntLE(data)
	{
		let r = 0;
		for(let i = 0; i < 4; i++) r += data[this._ptr++] << (8*i);
		return r;
	}

	getIntBE(data)
	{
		let r = 0;
		for(let i = 0; i < 4; i++) r += data[this._ptr++] << (8*(3-i));
		return r;
	}

	getShortBE(data)
	{
		let r = 0;
		for(let i = 0; i < 2; i++) r += data[this._ptr++] << (8*(1-i));
		return r;
	}

	getStr(data)
	{
		let c, r = '';
		while(c = data[this._ptr++]) r += String.fromCharCode(c);
		return r;
	}

	makeStr(data, start, len)
	{
		let r = '';
		for (let i = 0; i < len; i++) {
			r += String.fromCharCode(data[start+i]);
		}
		return r;
	}

	framesToText(frameData)
	{
		let text = '';
		for (let i = 0 ; i < frameData.length; i++) {
			for (let j = 0; j < 15; j++) {
				text += '' + frameData[i][j] + ' ';
			}
			text += '' + frameData[i][15] + '\n';
		}
		return text;
	}

	saveText(header, textData)
	{
		let data = '';

		// header
		Object.keys(header).sort().forEach(function(key) {
			data += '' + key + ' ' + header[key] + '\n';
		});
		// data
		data += 'frame_data\n' + textData;

		if (!("TextEncoder" in window))
			alert("Sorry, this browser does not support TextEncoder...");

		let enc = new TextEncoder(); // always utf-8
		return enc.encode(data);	// Uint8Array
	}
}


// should be equivalent to original fym_to_text.py
class FymConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	convert(data)
	{
		// known limitation: there seem to be .fym files for 2-chip configurations
		// see files marked as 'ts' on http://ym.mmcm.ru - respective support HAS NOT
		// been implemented here (I did not find any 'ts' songs that seem to be worth the trouble)

		if (typeof window.pako == 'undefined') { alert("ERROR unresolved pako library dependency"); }

		let fymData = window.pako.inflate(new Uint8Array(data));

		let fymHeaderSize = this.getIntLE(fymData);
		let frameCount = this.getIntLE(fymData);
		let loopFrame = this.getIntLE(fymData);
		let clockRate = this.getIntLE(fymData);
		let frameRate = this.getIntLE(fymData);

		this.caller.setTrackInfo(this.getStr(fymData), this.getStr(fymData), "");

		let header = {
		  'pan_a': 10,
		  'pan_b': 50,
		  'pan_c': 90,
		  'volume': 50
		};
		header['frame_count'] = frameCount;
		header['clock_rate'] = clockRate;
		header['frame_rate'] = frameRate;

		let frameData = []
		for (let i = 0; i < frameCount; i++) {
			frameData.push(new Array(16).fill(0));
		}
		for (let i = 0; i < 14; i++) {
			for (let j = 0; j < frameCount; j++) {
				frameData[j][i] = fymData[fymHeaderSize + i * frameCount + j];
			}
		}
		let textData = this.framesToText(frameData);
		return this.saveText(header, textData);
	}
}


// should be equivalent to original ym_to_text.py (with added LHA handling)
class YmConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	convert(data)
	{
		let id = this.makeStr(data, 2, 3); 		// handle LHA compression
		if ((id == '-lh') || (id == '-lz')) {
			let lha = new LhaReader(new LhaArrayReader(data));	// note: modified 'extract' version
			data = lha.extract(null, function(done, total) {
	//			console.log('Extracting LHA data: ' + (done / total * 100) + '% complete.');
			});
		}

		if (this.makeStr(data, 4, 8) != 'LeOnArD!') return null;

		let version = this.makeStr(data, 0, 3);

		if ((version == 'YMT') || (version == 'MIX'))
		{
			console.log("pure sample replay file format not supported here: " + this.makeStr(data, 0, 4));
			return null;
		}
		else if ((version != 'YM5') && (version != 'YM6'))
		{
			console.log("old .ym file format not supported here: " + this.makeStr(data, 0, 4));
			return null;
		}

		this._ptr = 12;	// skip 12 bytes

		let frameCount = this.getIntBE(data);
		let interleaved = this.getIntBE(data) & 1;
		let sampleCount = this.getShortBE(data);		// digi drum samples

		if (sampleCount) console.log("WARNING: YM digi drums not implemented");

		let clockRate = this.getIntBE(data);
		let frameRate = this.getShortBE(data);
		let loopFrame = this.getIntBE(data);
		let additionalSize = this.getShortBE(data);

		this._ptr = 34;	// reset to start of sample data (noop)

		for (let i = 0; i < sampleCount; i++) {
			let sampleSize = this.getIntBE(data);
			this._ptr += sampleSize;
		}

		this.caller.setTrackInfo(this.getStr(data), this.getStr(data), this.getStr(data));

		let headerSize = this._ptr; // now points to the recorded YM register data

		let header = {
		  'pan_a': 10,
		  'pan_b': 50,
		  'pan_c': 90,
		  'volume': 50
		};
		header['frame_count'] = frameCount;
		header['clock_rate'] = clockRate;
		header['frame_rate'] = frameRate;

		let frameData = []
		for (let i = 0; i < frameCount; i++) {
			frameData.push(new Array(16).fill(0));
		}

		let rowInSize = 16;
		let rowOutSize = 14;	// note: the .ym extra features are not supported here

		if (interleaved)
		{
			for (let i = 0; i < rowOutSize; i++) {
				for (let j = 0; j < frameCount; j++) {
					frameData[j][i] = data[headerSize + i * frameCount + j];
				}
			}
		}
		else
		{
			for (let i = 0; i < rowOutSize; i++) {
				for (let j = 0; j < frameCount; j++) {
					frameData[j][i] = data[headerSize + j * rowInSize + i];
				}
			}
		}
		let textData = this.framesToText(frameData);
		return this.saveText(header, textData);
	}
}


	// should be equivalent to original psg_to_text.py
class PSGConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	frameToTxt(r)
	{
		if (r[13] != this.old_shape)
		{
			this.old_shape = r[13];
		}
		else
		{
			r[13] = 255;
		}
		return ''+r[0]+' '+r[1]+' '+r[2]+' '+r[3]+' '+r[4]+' '+r[5]+' '+r[6]+' '+r[7]+' '+
				r[8]+' '+r[9]+' '+r[10]+' '+r[11]+' '+r[12]+' '+r[13]+' '+r[14]+' '+r[15]+'\n';
	}

	convert(data)
	{
		if(this.makeStr(data, 0, 4) != 'PSG\x1a') return null;

		let header = {
		  'pan_a': 10,
		  'pan_b': 50,
		  'pan_c': 90,
		  'volume': 50
		};

		let r = new Array(16).fill(0);
		this.old_shape = 255;
		let frameData = '';

		header['frame_count'] = 0;

		let index = 0;
		while (index < data.length) {
			let command = data[index];
			index += 1;
			if (command < 16) {
				r[command] = data[index];
				index += 1;
			} else if (command == 0xfd) {
				break;
			} else if (command == 0xff) {
				frameData += this.frameToTxt(r);
				header['frame_count'] += 1;
			} else if (command == 0xfe) {
				let count = data[index];
				index += 1;
				for (let i= 0; i<count * 4; i++) {
					frameData += this.frameToTxt(r);
					header['frame_count'] += 1;
				}
			}
		}
		return this.saveText(header, frameData);
	}
}


// should be equivalent to original afx_to_text.py
class AFXConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	frameToTxt(p1, p2, p3, p4, p5)
	{
		return ''+p1+' '+p2+' 0 0 0 0 '+p3+' '+p4+' '+p5+' 0 0 0 0 255 0 0\n';
	}

	convert(data)
	{
		let header = {
		  'pan_a': 50,
		  'pan_b': 0,
		  'pan_c': 0,
		  'volume': 140
		};

		let volume = 0;
		let tone = 0;
		let noise = 0;
		let tOff = 0;
		let nOff = 0;
		let status = 0;
		let frameData = '';

		header['frame_count'] = 0

		let index = 0;
		while (index < data.length) {
			status = data[index];
			volume = status & 0xf;
			tOff = (status & 0x10) != 0;
			nOff = (status & 0x80) != 0;
			index += 1;
			if (status & 0x20)
			{
				tone = data[index] | (data[index + 1] << 8);
				index += 2;
			}
			if (status & 0x40)
			{
				noise = data[index];
				index += 1;
			}
			if (noise == 0x20)
			{
				break;
			}
			frameData += this.frameToTxt(tone & 0xff, (tone >> 8) & 0xf, noise, tOff | (nOff << 3), volume);
			header['frame_count'] += 1
		}

		return this.saveText(header, frameData);
	}
}

class AyumiBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(scopeEnabled)
	{
		super(backend_AYUMI.Module, 2, new SimpleFileMapper(backend_AYUMI.Module),
						new HEAP16ScopeProvider(backend_AYUMI.Module, 0x8000));

		this._scopeEnabled = scopeEnabled;

		this.ensureReadyNotification();
	}

	convert2text(filename, data)
	{
		this.trackName = filename.split('/').pop();
		this.authorName = "";
		this.trackComment= "";

		// ayumi uses some proprietary "text" format and any input is
		// first converted to that format (corresponds to the .py scripts in the original code)

		if (filename.endsWith(".fym"))
		{
			return new FymConverter(this).convert(data);
		}
		else if (filename.endsWith(".ym"))
		{
			return new YmConverter(this).convert(data);
		}
		else if (filename.endsWith(".psg"))
		{
			return new PSGConverter(this).convert(data);
		}
		else if (filename.endsWith(".afx"))
		{
			return new AFXConverter(this).convert(data);
		}
		else if (filename.endsWith(".text"))
		{
			return data;
		}
		return null;
	}

	setTrackInfo(trackName, authorName, comment)
	{
		this.trackName = trackName;
		this.authorName = authorName;
		this.trackComment = comment;
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);

		// fixem: fragile name based detection.. better check the magic number in the file data (e.g. for "ZXAYAMAD")
		this.type = filename.endsWith(".ay") || filename.endsWith(".amad") || filename.endsWith(".fxm"); // AMAD or Fuxoft files

		if (!this.type)
		{
			data = this.convert2text(filename, data);	// original recorded data approach
		}
		if (data == null) return 1;

		return this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), 1024, this._scopeEnabled);
	}

	getSongInfoMeta()
	{
		return {
			songName: String,
			songAuthor: String
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		if (this.type)
		{
			let numAttr= 2;
			let ret = this.Module.ccall('emu_get_track_info', 'number');

			let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);

			result.songName = this.Module.Pointer_stringify(array[0]);
			if (!result.songName.length) result.songName = this._makeTitleFromPath(filename);

			result.songAuthor = this.Module.Pointer_stringify(array[1]);
		}
		else
		{
			// song infos are no available in ayumi's TEXT format
			// so the info is extracted here while converting the input
			result.songName = this.trackName;
			result.songAuthor = this.authorName;
		}
	}
};
