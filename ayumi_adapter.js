/*
 ayumi_adapter.js: Adapts ayumi backend to generic WebAudio/ScriptProcessor player.

   version 1.1
   copyright (C) 2018-2023 Juergen Wothke

*/

class AyumiConverterBase {
	constructor()
	{
		this._ptr = 0;
	}

	getIntLE(data)
	{
		let r = 0;
		for(let i = 0; i < 4; i++) r += data[this._ptr++] << (8*i);
		return r;
	}

	getIntBE(data)
	{
		let r = 0;
		for(let i = 0; i < 4; i++) r += data[this._ptr++] << (8*(3-i));
		return r;
	}

	getShortBE(data)
	{
		let r = 0;
		for(let i = 0; i < 2; i++) r += data[this._ptr++] << (8*(1-i));
		return r;
	}

	getStr(data)
	{
		let c, r = '';
		while(c = data[this._ptr++]) r += String.fromCharCode(c);
		return r;
	}

	makeStr(data, start, len)
	{
		let r = '';
		for (let i = 0; i < len; i++) {
			r += String.fromCharCode(data[start+i]);
		}
		return r;
	}

	framesToText(frameData)
	{
		let text = '';
		for (let i = 0 ; i < frameData.length; i++) {
			for (let j = 0; j < 15; j++) {
				text += '' + frameData[i][j] + ' ';
			}
			text += '' + frameData[i][15] + '\n';
		}
		return text;
	}

	saveText(header, textData)
	{
		let data = '';

		// header
		Object.keys(header).sort().forEach(function(key) {
			data += '' + key + ' ' + header[key] + '\n';
		});
		// data
		data += 'frame_data\n' + textData;

		if (!("TextEncoder" in window))
			alert("Sorry, this browser does not support TextEncoder...");

		let enc = new TextEncoder(); // always utf-8
		return enc.encode(data);	// Uint8Array
	}
}


// should be equivalent to original fym_to_text.py
class FymConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	convert(data)
	{
		// known limitation: there seem to be .fym files for 2-chip configurations
		// see files marked as 'ts' on http://ym.mmcm.ru - respective support HAS NOT
		// been implemented here (I did not find any 'ts' songs that seem to be worth the trouble)

		if (typeof window.pako == 'undefined') { alert("ERROR unresolved pako library dependency"); }

		let fymData = window.pako.inflate(new Uint8Array(data));

		let fymHeaderSize = this.getIntLE(fymData);
		let frameCount = this.getIntLE(fymData);
		let loopFrame = this.getIntLE(fymData);
		let clockRate = this.getIntLE(fymData);
		let frameRate = this.getIntLE(fymData);

		this.caller.setTrackInfo(this.getStr(fymData), this.getStr(fymData), "");

		let header = {
		  'pan_a': 10,
		  'pan_b': 50,
		  'pan_c': 90,
		  'volume': 50
		};
		header['frame_count'] = frameCount;
		header['clock_rate'] = clockRate;
		header['frame_rate'] = frameRate;

		let frameData = []
		for (let i = 0; i < frameCount; i++) {
			frameData.push(new Array(16).fill(0));
		}
		for (let i = 0; i < 14; i++) {
			for (let j = 0; j < frameCount; j++) {
				frameData[j][i] = fymData[fymHeaderSize + i * frameCount + j];
			}
		}
		let textData = this.framesToText(frameData);
		return this.saveText(header, textData);
	}
}


// should be equivalent to original ym_to_text.py (with added LHA handling)
class YmConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	convert(data)
	{
		let id = this.makeStr(data, 2, 3); 		// handle LHA compression
		if ((id == '-lh') || (id == '-lz')) {
			let lha = new LhaReader(new LhaArrayReader(data));	// note: modified 'extract' version
			data = lha.extract(null, function(done, total) {
	//			console.log('Extracting LHA data: ' + (done / total * 100) + '% complete.');
			});
		}

		if (this.makeStr(data, 4, 8) != 'LeOnArD!') return null;

		let version = this.makeStr(data, 0, 3);

		if ((version == 'YMT') || (version == 'MIX'))
		{
			console.log("pure sample replay file format not supported here: " + this.makeStr(data, 0, 4));
			return null;
		}
		else if ((version != 'YM5') && (version != 'YM6'))
		{
			console.log("old .ym file format not supported here: " + this.makeStr(data, 0, 4));
			return null;
		}

		this._ptr = 12;	// skip 12 bytes

		let frameCount = this.getIntBE(data);
		let interleaved = this.getIntBE(data) & 1;
		let sampleCount = this.getShortBE(data);		// digi drum samples

		if (sampleCount) console.log("WARNING: YM digi drums not implemented");

		let clockRate = this.getIntBE(data);
		let frameRate = this.getShortBE(data);
		let loopFrame = this.getIntBE(data);
		let additionalSize = this.getShortBE(data);

		this._ptr = 34;	// reset to start of sample data (noop)

		for (let i = 0; i < sampleCount; i++) {
			let sampleSize = this.getIntBE(data);
			this._ptr += sampleSize;
		}

		this.caller.setTrackInfo(this.getStr(data), this.getStr(data), this.getStr(data));

		let headerSize = this._ptr; // now points to the recorded YM register data

		let header = {
		  'pan_a': 10,
		  'pan_b': 50,
		  'pan_c': 90,
		  'volume': 50
		};
		header['frame_count'] = frameCount;
		header['clock_rate'] = clockRate;
		header['frame_rate'] = frameRate;

		let frameData = []
		for (let i = 0; i < frameCount; i++) {
			frameData.push(new Array(16).fill(0));
		}

		let rowInSize = 16;
		let rowOutSize = 14;	// note: the .ym extra features are not supported here

		if (interleaved)
		{
			for (let i = 0; i < rowOutSize; i++) {
				for (let j = 0; j < frameCount; j++) {
					frameData[j][i] = data[headerSize + i * frameCount + j];
				}
			}
		}
		else
		{
			for (let i = 0; i < rowOutSize; i++) {
				for (let j = 0; j < frameCount; j++) {
					frameData[j][i] = data[headerSize + j * rowInSize + i];
				}
			}
		}
		let textData = this.framesToText(frameData);
		return this.saveText(header, textData);
	}
}


	// should be equivalent to original psg_to_text.py
class PSGConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	frameToTxt(r)
	{
		if (r[13] != this.old_shape)
		{
			this.old_shape = r[13];
		}
		else
		{
			r[13] = 255;
		}
		return ''+r[0]+' '+r[1]+' '+r[2]+' '+r[3]+' '+r[4]+' '+r[5]+' '+r[6]+' '+r[7]+' '+
				r[8]+' '+r[9]+' '+r[10]+' '+r[11]+' '+r[12]+' '+r[13]+' '+r[14]+' '+r[15]+'\n';
	}

	convert(data)
	{
		if(this.makeStr(data, 0, 4) != 'PSG\x1a') return null;

		let header = {
		  'pan_a': 10,
		  'pan_b': 50,
		  'pan_c': 90,
		  'volume': 50
		};

		let r = new Array(16).fill(0);
		this.old_shape = 255;
		let frameData = '';

		header['frame_count'] = 0;

		let index = 0;
		while (index < data.length) {
			let command = data[index];
			index += 1;
			if (command < 16) {
				r[command] = data[index];
				index += 1;
			} else if (command == 0xfd) {
				break;
			} else if (command == 0xff) {
				frameData += this.frameToTxt(r);
				header['frame_count'] += 1;
			} else if (command == 0xfe) {
				let count = data[index];
				index += 1;
				for (let i= 0; i<count * 4; i++) {
					frameData += this.frameToTxt(r);
					header['frame_count'] += 1;
				}
			}
		}
		return this.saveText(header, frameData);
	}
}


// should be equivalent to original afx_to_text.py
class AFXConverter extends AyumiConverterBase {
	constructor(caller)
	{
		super();
		this.caller = caller;
	}

	frameToTxt(p1, p2, p3, p4, p5)
	{
		return ''+p1+' '+p2+' 0 0 0 0 '+p3+' '+p4+' '+p5+' 0 0 0 0 255 0 0\n';
	}

	convert(data)
	{
		let header = {
		  'pan_a': 50,
		  'pan_b': 0,
		  'pan_c': 0,
		  'volume': 140
		};

		let volume = 0;
		let tone = 0;
		let noise = 0;
		let tOff = 0;
		let nOff = 0;
		let status = 0;
		let frameData = '';

		header['frame_count'] = 0

		let index = 0;
		while (index < data.length) {
			status = data[index];
			volume = status & 0xf;
			tOff = (status & 0x10) != 0;
			nOff = (status & 0x80) != 0;
			index += 1;
			if (status & 0x20)
			{
				tone = data[index] | (data[index + 1] << 8);
				index += 2;
			}
			if (status & 0x40)
			{
				noise = data[index];
				index += 1;
			}
			if (noise == 0x20)
			{
				break;
			}
			frameData += this.frameToTxt(tone & 0xff, (tone >> 8) & 0xf, noise, tOff | (nOff << 3), volume);
			header['frame_count'] += 1
		}

		return this.saveText(header, frameData);
	}
}

class AyumiBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(scopeEnabled)
	{
		super(backend_AYUMI.Module, 2, new SimpleFileMapper(backend_AYUMI.Module),
						new HEAP16ScopeProvider(backend_AYUMI.Module, 0x8000));

		this._scopeEnabled = scopeEnabled;

		this.ensureReadyNotification();
	}

	convert2text(filename, data)
	{
		this.trackName = filename.split('/').pop();
		this.authorName = "";
		this.trackComment= "";

		// ayumi uses some proprietary "text" format and any input is
		// first converted to that format (corresponds to the .py scripts in the original code)

		if (filename.endsWith(".fym"))
		{
			return new FymConverter(this).convert(data);
		}
		else if (filename.endsWith(".ym"))
		{
			return new YmConverter(this).convert(data);
		}
		else if (filename.endsWith(".psg"))
		{
			return new PSGConverter(this).convert(data);
		}
		else if (filename.endsWith(".afx"))
		{
			return new AFXConverter(this).convert(data);
		}
		else if (filename.endsWith(".text"))
		{
			return data;
		}
		return null;
	}

	setTrackInfo(trackName, authorName, comment)
	{
		this.trackName = trackName;
		this.authorName = authorName;
		this.trackComment = comment;
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);

		// fixem: fragile name based detection.. better check the magic number in the file data (e.g. for "ZXAYAMAD")
		this.type = filename.endsWith(".ay") || filename.endsWith(".amad") || filename.endsWith(".fxm"); // AMAD or Fuxoft files

		if (!this.type)
		{
			data = this.convert2text(filename, data);	// original recorded data approach
		}
		if (data == null) return 1;

		return this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), 1024, this._scopeEnabled);
	}

	getSongInfoMeta()
	{
		return {
			songName: String,
			songAuthor: String
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		if (this.type)
		{
			let numAttr= 2;
			let ret = this.Module.ccall('emu_get_track_info', 'number');

			let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);

			result.songName = this.Module.Pointer_stringify(array[0]);
			if (!result.songName.length) result.songName = this._makeTitleFromPath(filename);

			result.songAuthor = this.Module.Pointer_stringify(array[1]);
		}
		else
		{
			// song infos are no available in ayumi's TEXT format
			// so the info is extracted here while converting the input
			result.songName = this.trackName;
			result.songAuthor = this.authorName;
		}
	}
};
