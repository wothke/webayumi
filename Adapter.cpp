/**
* Provides the APIs needed by the WebAudio based player.
*
 	Copyright (C) 2018-2023 Juergen Wothke
*/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <stdint.h>

#include "emscripten.h"

extern "C" {
#include "ayumi.h"
#include "load_text.h"

extern void fxm_init(const uint8_t *fxm, uint32_t len);
extern void fxm_loop();
extern int*  fxm_get_registers();
extern void fxm_get_songinfo(const char** songName, const char **songAuthor);
}

#include "MetaInfoHelper.h"
using emsutil::MetaInfoHelper;

#define WAVE_FORMAT_IEEE_FLOAT 3

namespace aym {
	class Adapter {
	public:
		Adapter() : _numSamples(0), _soundBufferLen(0), _soundBuffer(NULL), _numberOfSamplesRendered(0),
					_scopesEnabled(0), _voice1Buffer(NULL), _voice2Buffer(NULL), _voice3Buffer(NULL),
					_frame(0), _isrCounter(1)
		{
		}

		int loadFile(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: audioBufSize not implemented (see _numSamples)

			_scopesEnabled = scopesEnabled;
			_type = getType(std::string(filename));	// _type=0 means original "recorded data" approch

			setDefaultTextData(sampleRate, &_t);

			if (_type == 0) 	// replay recorded data
			{
				if(!load_text_buffer((char*)inBuffer, inBufSize, &_t))
				{
					return 1;	// error
				}

				int sampleCount = (int) ((_t.sample_rate / _t.frame_rate) * _t.frame_count);
				if (sampleCount == 0)
				{
					return 1;	// no frames
				}
			}
			else 	// use FXM player to generate soundchip settings
			{
				// just use the soundchip emu while feeding updates via FXM player

				_t.is_ym = 0;
				_t.pan[0] = 0.8;
				_t.pan[1] = 0.2;
				_t.pan[2] = 0.5;
				_t.volume = 0.7;
		//		_t.eqp_stereo_on = n;
		//		_t.dc_filter_on = n;

				fxm_init((const uint8_t*)inBuffer, inBufSize);

				const char* songName;
				const char* songAuthor;
				fxm_get_songinfo(&songName, &songAuthor);

				MetaInfoHelper *info = MetaInfoHelper::getInstance();
				info->setText(0, songName, "");
				info->setText(1, songAuthor, "");
			}

			if (!ayumi_configure(&_ay, _t.is_ym, _t.clock_rate, _t.sample_rate))
			{
				return 1;	// ayumi_configure error (wrong sample rate?)
			}
			if (_t.pan[0] >= 0)
			{
				ayumi_set_pan(&_ay, 0, _t.pan[0], _t.eqp_stereo_on);
			}
			if (_t.pan[1] >= 0)
			{
				ayumi_set_pan(&_ay, 1, _t.pan[1], _t.eqp_stereo_on);
			}
			if (_t.pan[2] >= 0)
			{
				ayumi_set_pan(&_ay, 2, _t.pan[2], _t.eqp_stereo_on);
			}
			_frame = 0;

			return 0;
		}

		void teardown()
		{
			MetaInfoHelper::getInstance()->clear();
		}

		int getSampleRate()
		{
			return _t.sample_rate;
		}

		uint32_t getSampleBufferLength()
		{
			return _numberOfSamplesRendered;
		}

		char* getSampleBuffer()
		{
			return (char*) _soundBuffer;
		}

		uint32_t getMaxPosition()
		{
			return _t.frame_count * 1000 / _t.frame_rate;
		}

		uint32_t getCurrentPosition()
		{
			return _frame * 1000 / _t.frame_rate;
		}

		void seekPosition(uint32_t ms)
		{
			_frame = ms * _t.frame_rate / 1000;
		}

		void setSongParams(int clockRate, int frameCount, double frameRate)
		{
			_t.clock_rate= clockRate;
			_t.frame_count= frameCount;
			_t.frame_rate= frameRate;

			setupAudioOut();
		}

		int genSamples() {
			_numberOfSamplesRendered = 0;

			if (_frame < _t.frame_count)
			{
				int16_t* out= _soundBuffer;


				while ((_numberOfSamplesRendered < _numSamples) && (_frame < _t.frame_count)) {
					_isrCounter += _isrStep;
					if (_isrCounter >= 1)
					{
						_isrCounter -= 1;

						int* regs;
						if(_type != 0)
						{
							fxm_loop();
							regs= fxm_get_registers();
						}
						else
						{
							regs= &_t.frame_data[_frame * 16]; // original Ayumi
						}

						updateAyumiState(&_ay, regs);
						_frame += 1;
					}
					ayumi_process(&_ay);
					if (_t.dc_filter_on)
					{
						ayumi_remove_dc(&_ay);
					}

					if (_scopesEnabled)
					{
						_voice1Buffer[_numberOfSamplesRendered] = getVoiceOutput(&_ay, 0);
						_voice2Buffer[_numberOfSamplesRendered] = getVoiceOutput(&_ay, 1);
						_voice3Buffer[_numberOfSamplesRendered] = getVoiceOutput(&_ay, 2);;
					}
					out[0] = (int16_t) (_ay.left * _t.volume * 0x7fff);
					out[1] = (int16_t) (_ay.right * _t.volume * 0x7fff);
					out += 2;

					_numberOfSamplesRendered++;
				}
				int fill = _numSamples - _numberOfSamplesRendered;
				for (int i= 0; i < fill; i++) {
					out[0] = out[1] = 0;
					out += 2;
					_numberOfSamplesRendered++;
				}
			}
			return (_numberOfSamplesRendered <= 0) ? 1 : 0; // >0 means "end song"
		}

		int getNumberTraceStreams() {
			return _scopesEnabled ? 3 : 0;
		}

		const char** getTraceStreams() {
			return _scopesEnabled ? (const char**)_scopeBuffers : 0;
		}

	private:
		void setupAudioOut()
		{
			uint32_t numSamples = _t.sample_rate / _t.frame_rate;

			if (numSamples > _soundBufferLen) {
				if (_soundBuffer) free(_soundBuffer);
				if (_voice1Buffer) free(_voice1Buffer);
				if (_voice2Buffer) free(_voice2Buffer);
				if (_voice3Buffer) free(_voice3Buffer);

				_soundBuffer = (int16_t*)malloc(sizeof(int16_t)*numSamples * 2);
				_voice1Buffer = (int16_t*)malloc(sizeof(int16_t)*numSamples);
				_voice2Buffer = (int16_t*)malloc(sizeof(int16_t)*numSamples);
				_voice3Buffer = (int16_t*)malloc(sizeof(int16_t)*numSamples);

				_scopeBuffers[0] = _voice1Buffer;
				_scopeBuffers[1] = _voice2Buffer;
				_scopeBuffers[2] = _voice3Buffer;

				_soundBufferLen = numSamples;
			}
			_numSamples = numSamples;

			_isrStep = _t.frame_rate / _t.sample_rate;
			_isrCounter = 1;
		}

		void setDefaultTextData(int sampleRate, struct text_data* t)
		{
			if (t->frame_data) free(t->frame_data);	// cleanup last song

			memset(t, 0, sizeof(struct text_data));
			t->sample_rate = sampleRate;
			t->eqp_stereo_on = 0;
			t->dc_filter_on = 1;
			t->is_ym = 1;
			t->clock_rate = 2000000;
			t->frame_rate = 50;

			setupAudioOut();
		}

		void updateAyumiState(struct ayumi* ay, int* r) {
			ayumi_set_tone(ay, 0, (r[1] << 8) | r[0]);
			ayumi_set_tone(ay, 1, (r[3] << 8) | r[2]);
			ayumi_set_tone(ay, 2, (r[5] << 8) | r[4]);
			ayumi_set_noise(ay, r[6]);
			ayumi_set_mixer(ay, 0, r[7] & 1, (r[7] >> 3) & 1, r[8] >> 4);
			ayumi_set_mixer(ay, 1, (r[7] >> 1) & 1, (r[7] >> 4) & 1, r[9] >> 4);
			ayumi_set_mixer(ay, 2, (r[7] >> 2) & 1, (r[7] >> 5) & 1, r[10] >> 4);
			ayumi_set_volume(ay, 0, r[8] & 0xf);
			ayumi_set_volume(ay, 1, r[9] & 0xf);
			ayumi_set_volume(ay, 2, r[10] & 0xf);
			ayumi_set_envelope(ay, (r[12] << 8) | r[11]);
			if (r[13] != 255)
			{
				ayumi_set_envelope_shape(ay, r[13]);
			}
		}

		bool endsWith(const std::string& str, const std::string& suffix)
		{
			return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
		}

		int getType(const std::string& fn)
		{
			return	endsWith(fn, std::string(".ay")) ||
					endsWith(fn, std::string(".amad")) ||
					endsWith(fn, std::string(".fxm"));		// AMAD or Fuxoft files
		}

		int16_t getVoiceOutput(struct ayumi* ay, int voice)
		{
			int noise = ay->noise & 1;

			// note: in test-song for voices 1 + 3 it is just the volume that is displayed.. whereas for
			// voice 2 it initially shows the envelope..
			int envelope = ay->channels[voice].e_on ? ay->envelope : ay->channels[voice].volume * 2 + 1; // 5-bit "envelope" 0 to 31

			int out = ((ay->channels[voice].tone | ay->channels[voice].t_off) & (noise | ay->channels[voice].n_off));	//
			double d = ay->dac_table[envelope];	// range is 0.0f .. 1.0f

			out = out ? -(0x8000) : 0x7fff; 	// center

			return  round(d*out);
		}
	private:
		uint32_t _numSamples;
		uint32_t _soundBufferLen;
		int16_t *_soundBuffer;
		uint32_t _numberOfSamplesRendered;

		// output the realtime volume info
		int32_t _scopesEnabled;

		int16_t *_voice1Buffer;
		int16_t *_voice2Buffer;
		int16_t *_voice3Buffer;

		int16_t *_scopeBuffers[3];

		struct text_data _t;
		struct ayumi _ay;

		uint32_t _type;
		int _frame;
		double _isrStep;
		double _isrCounter;	// keep overflows between calls
	};
};

aym::Adapter _adapter;

// callback used by fxm_init
extern "C" void set_song_params(int clock_rate, int frame_count, double frame_rate) {
	_adapter.setSongParams(clock_rate, frame_count, frame_rate);
}


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

// --- standard functions
EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return 0;	/*there are no subsongs*/ }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))			{ _adapter.seekPosition(ms);  }
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }
EMBIND(int, emu_number_trace_streams())				{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())		{ return _adapter.getTraceStreams(); }
